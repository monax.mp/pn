$(document).ready(function () {

    if ($(window).width() > 991) {
        $('.navbar-collapse .navbar-nav > li.dropdown').hover(function () {
            $('ul.dropdown-menu', this).stop(true, true).slideDown('fast');
            $(this).addClass('open');
        }, function () {
            $('ul.dropdown-menu', this).stop(true, true).slideUp('fast');
            $(this).removeClass('open');
        });

        $('.navbar-collapse .navbar-nav > li.dropdown').on('click', '.dropdown-toggle', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');

            window.location.href = url;
        });
    } else {
        return false;
    }
});

$(document).ready(function(){

    if ($(window).width() > 991) {

        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
            } else {
            $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });

    } else {
        return false;
    }


});

jQuery(document).ready(function($) {

    $('.js-img-wrapper').each(function() {

        var imgWrap =  $(this).children('img').attr('src');

        $(this).children('img').css('visibility', 'hidden');

        $(this).css('background-image', 'url(' + imgWrap + ')');

    });

});

//  Owl-carousel

jQuery(document).ready(function($) {

    $('.service-carousel > .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        items: 1,
        smartSpeed:450,
    });

    $('.portfolio-carousel > .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<div class='nav-prev'><div>", "<div class='nav-next'><div>"],
        dots: false,
        items: 1,
        smartSpeed:450,
    });

    $('.event-carousel > .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<div class='nav-prev'><div>", "<div class='nav-next'><div>"],
        dots: true,
        items: 1,
        smartSpeed:450,
    });

    $('.promo-slider .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<img src='/css/img/arrow-next.png' alt='arrow-next' />", "<img src='/css/img/arrow-prev.png' alt='arrow-prev' />"],
        dots: true,
        items: 1,
        smartSpeed:450,
    });

    $('.owl-dots-numb .owl-dot > span').each(function(index) {
        $(this).text(index + 1);
    });

});


(function($) {
    $(function() {

        // Проверим, есть ли запись в куках о посещении посетителя
        // Если запись есть - ничего не делаем
        //if (!$.cookie('was')) {
            // console.log(divs);
            $.each($('.home-page #content .container > div'), function () {
                // Скрываем основные блоки и футер
                $(this).css('display', 'none');
            });
            $('.home-page footer').css('display', 'none');
            // Скрываем navbar в постоянном виде
            $('.home-page .navbar').css('display', 'none');
            // И показываем сайдбар для первого посещения
            $('.home-page .navbar-first').removeClass('hidden');
            // Отображаем главный экран и меню при первом посещении
            $('.home-page #content-first').css('display', 'block');
            $('.home-page #content').css('background-color', '#ffffff');
            $('.home-page #content').addClass('first-visit');
            $('.home-page .menu').css('display', 'block');
            $('.home-page .expand').css('display', 'block');
        //}
    })
})(jQuery);


$(document).on('ready', function () {
    $('#stock-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        items: 1,
        smartSpeed:450,
    });

    // $('#resetCookies').on('click', function () {
    //     $.cookie('was','was', {expires: 0});
    // });

    // По клику на кнопку

    // $('#expand-page').on('click', function () {
    //     $('.home-page #content-first').slideUp(500);
    //     $('.home-page .navbar-first').slideUp(500);
    //     setTimeout(function () {
    //         $('.home-page .navbar').fadeIn(500);
    //         $.each($('.home-page #content .container > div'),function (i) {
    //             var div = $(this);
    //             if (!div.hasClass('expand')) {
    //                 setTimeout(function () {
    //                     div.slideDown().show(300);
    //                 }, (i + 1) * 100);
    //             }
    //         });

    //     }, 100);
    //     $('.home-page #content').css('background-color', 'rgb(245, 245, 245)');
    //     $('.home-page #content').removeClass('first-visit');
    //     $('.home-page .expand').css('display', 'none');
    //     $('.home-page footer').css('display', 'block');
    //     $.cookie('was','was', {expires: 7});
    // });


});


// По прокрутке мышки

var elem = document.getElementById('home-page');

if (elem) {
  if (elem.addEventListener) {
  if ('onwheel' in document) {
    // IE9+, FF17+
    elem.addEventListener ("wheel", onWheel, false);
  } else if ('onmousewheel' in document) {
    // устаревший вариант события
    elem.addEventListener ("mousewheel", onWheel, false);
  } else {
    // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
    elem.addEventListener ("MozMousePixelScroll", onWheel, false);
  }
} else { // IE<9
  elem.attachEvent ("onmousewheel", onWheel);
}
};

function onWheel(e) {
  e = e || window.event;

  // wheelDelta не дает возможность узнать количество пикселей
  var delta = e.deltaY || e.detail || e.wheelDelta;

    if (delta > 0) {

        $('.home-page #content-first').slideUp(500);
        $('.home-page .navbar-first').slideUp(500);
        setTimeout(function () {
            $('.home-page .navbar').fadeIn(500);
            $.each($('.home-page #content .container > div'),function (i) {
                var div = $(this);
                if (!div.hasClass('expand')) {
                    setTimeout(function () {
                        div.slideDown().show(300);
                    }, (i + 1) * 100);
                }
            });
            $('.home-page #content').css('background-color', 'rgb(245, 245, 245)');
            $('.home-page #content').removeClass('first-visit');
            $('.home-page .expand').css('display', 'none');


        }, 100);
        setTimeout(function(){
            $('.home-page footer').css('display', 'block');

            $('portfolio-block').addClass('show');
        }, 1000);

        setTimeout(function(){
            $('.portfolio-block').addClass('show');
        }, 2000);


        $.cookie('was','was', {expires: 7});
    }
}

//Animate CSS + WayPoints javaScript Plugin
//Example: $(".element").animated("zoomInUp");
(function($) {
    $.fn.animated = function(inEffect) {
        $(this).each(function() {
            var ths = $(this);
            ths.css("opacity", "0").addClass("animated").waypoint(function(dir) {
                if (dir === "down") {
                    ths.addClass(inEffect).css("opacity", "1");
                };
            }, {
                offset: "90%"
            });

        });
    };
})(jQuery);


// Анимация

jQuery(document).ready(function($) {

    $(".service-block .service-block-info").delay(2000).animated("slideInRight");

    $(".portfolio-block.show .item").animated("fadeIn");
    $(".portfolio-block-info.show").animated("fadeInUp");
    $(".portfolio-btn-wrapper.show").animated("flipInX");
    $(".about-block.show .counters").animated("fadeInRight");
    $(".about-block-info.show .title-item-extra_bold").animated("fadeInUp");
    $(".about-block-info.show .description").animated("fadeInUp");
    $(".about-block-info.show .btn-more").animated("fadeInUp");
    $(".sales-block.show .sales-block-info").animated("fadeInUp");
    $(".last-events-block.show .item.right-line").animated("fadeIn");
    $(".last-events-block.show .item").animated("fadeIn");
    $("article.item.load-effect.show").delay(2000).animated("fadeInUp");

});


jQuery(function($) {
    $('.portfolio-list .item').matchHeight();
    $('.command .item').matchHeight();
    $('.shop-category-list .item').matchHeight();
});


jQuery(document).ready(function($) {

    $('.search').on('click', '.search-link', function(event) {
        event.preventDefault();
        $('.search-text').toggleClass('hidden');
        $('.search-form').toggleClass('hidden');
        $('.search-link').toggleClass('active');
    });

    $('.shop-type-product .btn').click(function(event) {
        /* Act on the event */
        $('.shop-type-product .btn').removeClass('active');
        $(this).addClass('active');
    });
});

