<?php
class content_custom extends content{

	//public function getHtml($template = "default", $path = false, $limit = false, $ignore_paging = false, $i_need_deep = 0, $field_id = false, $asc = true) {
	public function getHtml($block = false) {
		if (!$block) return;
		$args = func_get_args();
		//var_dump($this->pid);exit();
		
		$template = 'htmlblock.xsl';
		$return = '';
		$block_arr = array();
		$block_arr['attribute:id'] = $block;
		$block_arr['attribute:pid'] = $this->pid;
		$lines = array();
		foreach ($args as $k => $param) {
			if ($k == 0) continue;
			$line_arr = array();
			$line_arr['attribute:id'] = $k;
			$line_arr['node:value'] = $param;
			$lines[] = $line_arr;
		}
		$total = count($lines);
		if ($total > 0) {
			$block_arr['subnodes:params'] = $lines;
		}
		$block_arr['count'] = $total;
		/*$data = array(
			'block' => array(
				'@id' =>  $block,
				'params' => $params
			)
		);*/
		
		$resourcesDir = cmsController::getInstance()->getResourcesDirectory();
		$oldResultMode = $this->isXSLTResultMode(true);
		
		list($tpl_block) = $this->loadTemplates($resourcesDir."/xslt/modules/content/".$template, "htmlblock");
		$return = $this->parseTemplateForMail($tpl_block, $block_arr);
		
		$this->isXSLTResultMode($oldResultMode);
		
		return $return;
	}
	
	public function getHtmlTemplate($block = false) {
		$data = self::getHtml($block);
		if (!$data) $data = '';
		$buffer = outputBuffer::current('HTTPOutputBuffer');
		$buffer->charset('utf-8');
		$buffer->contentType('text/html');
		$buffer->clear();
		$buffer->push($data);
		$buffer->end();
	}

};

?>