<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="total" />
	<xsl:template match="total[. &gt; ../per_page]">
		<xsl:apply-templates select="document(concat('udata://system/numpages/', ., '/', ../per_page,'///4'))" />
		<!-- <xsl:value-of select="concat('udata://system/numpages/', ., '/', ../per_page,'////4')" /> -->
	</xsl:template>
	
	<xsl:template match="udata[@method = 'numpages']" />
		
	
	<xsl:template match="udata[@method = 'numpages'][count(items)]">
	<!-- <xsl:copy-of select="." /> -->
		<div style="text-align:center;">
			<div class="numpage" style="width:auto; display:inline-block;">
				<xsl:if test="toprev_link">
					<a class="prev" href="{toprev_link}">&lt;</a>
				</xsl:if>
				
				<xsl:if test="tobegin_link and (tobegin_link/@page-num + 1) &lt;= items/item[1]/@page-num">
					<a href="{tobegin_link}">
						<xsl:value-of select="tobegin_link/@page-num + 1" />
					</a>
					<xsl:if test="(tobegin_link/@page-num + 1) &lt; items/item[1]/@page-num">
						<a class="dots">
							<xsl:text>...</xsl:text>	
						</a>
					</xsl:if>
				</xsl:if>
				<xsl:apply-templates select="items" mode="numpages"/>
				<xsl:if test="toend_link and (toend_link/@page-num - 1) &gt;= items/item[last()]/@page-num">
					<xsl:if test="(toend_link/@page-num - 1) &gt; items/item[last()]/@page-num">
						<a class="dots">
							<xsl:text>...</xsl:text>	
						</a>
					</xsl:if>
					<a href="{toend_link}">
						<xsl:value-of select="toend_link/@page-num + 1" />
					</a>
				</xsl:if>
				<xsl:if test="tonext_link">
					<a class="next" href="{tonext_link}">&gt;</a>
				</xsl:if>
				
				
				<!-- <xsl:apply-templates select="tobegin_link" />
				
				<xsl:apply-templates select="items/item" mode="numpages" />
				<xsl:apply-templates select="toend_link" /> -->
				<div class="clear"></div>
			</div>
		</div>
	</xsl:template>
	
	<xsl:template match="item" mode="numpages">
		<a href="{@link}">
       <!--  <xsl:if test="position()=1">
        <xsl:attribute name="style">display:none;</xsl:attribute>
        </xsl:if>
        <xsl:if test="position()=last()">
        <xsl:attribute name="style">display:none;</xsl:attribute>
        </xsl:if> -->
        <xsl:value-of select="." />
		</a>
	</xsl:template>
    
	
	<xsl:template match="item[@is-active = '1']" mode="numpages">
		<a style="background:#CCC;">
			<xsl:value-of select="." />
		</a>
	</xsl:template>
	
	<xsl:template match="tobegin_link">
		<a class="prev" href="{.}">
		<xsl:text>1</xsl:text>	
		</a>
        <a class="prev">
		<xsl:text>...</xsl:text>	
		</a>
	</xsl:template>
	
	<xsl:template match="toend_link">
        <a class="next">
		<xsl:text>...</xsl:text>	
		</a>
		<a class="next" href="{.}">
        <xsl:value-of select="@page-num+1" />
		</a>
	</xsl:template>

	<xsl:template match="item" mode="slider">
		<xsl:apply-templates select="preceding-sibling::item[1]" mode="slider_back" />
		<xsl:apply-templates select="following-sibling::item[1]" mode="slider_next" />
	</xsl:template>

	<xsl:template match="item" mode="slider_back">
		<a href="{@link}" title="&previous-page;" class="back" />
	</xsl:template>

	<xsl:template match="item" mode="slider_next">
		<a href="{@link}" title="&next-page;" class="next" />
	</xsl:template>

</xsl:stylesheet>