<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output encoding="utf-8" method="html" indent="yes" />

	<xsl:template match="status_notification">
        <xsl:text>Здравствуйте,</xsl:text>
        <xsl:variable select="document(concat('uobject://',order_id))//property[@name='customer_id']/value/item/@id" name="customer1"/>
		<span style="margin-left:10px;"></span>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='fname']/value" />
		<span style="margin-left:10px;"></span>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='father_name']/value" /><br /><br />
       <xsl:text> Спасибо за ваш заказ!  Наш оператор скоро свяжется с вами, чтобы организовать доставку и ответить на ваши вопросы.</xsl:text><br/>
       <xsl:text> Если вам нужна дополнительная информация, то пожалуйста пишите на: principnovoshop@yandex.ru или звоните Январь-Апрель С ПН-ПТ с 10:00 до 17:00 Март-Декабрь Ежедневно с 09:00 до 19:00. Контактый телефон для связи: +7(921) 952 91 90 или 8(812)702 92 93</xsl:text><br/><br/>
        <xsl:text>Номер вашего заказа:  </xsl:text>
		<xsl:value-of select="order_number" /><br/>
		<xsl:text>Статус вашего заказа: </xsl:text>
		<xsl:value-of select="status" />
        <br/>
        
        <div style=" float:left; padding:20px; border-top: 3px solid #964B00; border-left:1px solid #999;  border-right:1px solid #999; border-bottom:1px solid #999; width:40%;">
       <xsl:text>Контактные данные</xsl:text><br /><hr />
		
		<xsl:text>Имя:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='fname']/value" /><br />
		
		<xsl:text>E-mail:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='email']/value" /><br />
		<xsl:text>Телефон:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='phone']/value" /><br />
        <xsl:text>Желаемое время звонка:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='zhelaemoe_vremya_zvnoka']/value" /><br />
        <xsl:text>Адрес доставки:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='adres_dostavki']/value" /><br />
<xsl:text>Время доставки:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='vremya_dostavki']/value" /><br /><br />
        </div>
        
   <div style="clear:both;"></div>
      <xsl:apply-templates select="document(concat('udata://emarket/order/',order_id))" mode= "unic"/><br /><br /><br />
	<xsl:text>Благодарим Вас за покупку!</xsl:text><br />
    <xsl:text>С уважением, Администрация онлайн магазина principnovo.ru</xsl:text><br />
    <xsl:text> +7 (921) 952-91-90</xsl:text><br />
    <xsl:text>www.principnovo.ru</xsl:text>
	</xsl:template>

	<xsl:template match="status_notification_receipt">
      <xsl:text>Здравствуйте,</xsl:text>
        <xsl:variable select="document(concat('uobject://',order_id))//property[@name='customer_id']/value/item/@id" name="customer1"/>
		<span style="margin-left:10px;"></span>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='fname']/value" />
		<span style="margin-left:10px;"></span>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='father_name']/value" /><br /><br />
        <xsl:text> Спасибо за ваш заказ!  Наш оператор скоро свяжется с вами, чтобы организовать доставку и ответить на ваши вопросы.</xsl:text><br/>
       <xsl:text> Если вам нужна дополнительная информация, то пожалуйста пишите на: principnovoshop@yandex.ru или звоните в Пн.-Птн, 9:00 - 19:00 </xsl:text><br/><br/>
        <xsl:text>Номер вашего заказа:  </xsl:text>
		<xsl:value-of select="order_number" /><br/>
		<xsl:text>Статус вашего заказа: </xsl:text>
		<xsl:value-of select="status" /> <br/>
        
        <div style=" float:left; padding:20px; border-top: 3px solid #964B00; border-left:1px solid #999;  border-right:1px solid #999; border-bottom:1px solid #999; width:40%;">
       <xsl:text>Контактные данные</xsl:text><br /><hr />

		<xsl:text>Имя:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='fname']/value" /><br />
		
		<xsl:text>E-mail:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='email']/value" /><br />
		<xsl:text>Телефон:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='phone']/value" /><br />
        <xsl:text>Желаемое время звонка:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='zhelaemoe_vremya_zvnoka']/value" /><br />
        <xsl:text>Адрес доставки:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='adres_dostavki']/value" /><br />
<xsl:text>Время доставки:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='vremya_dostavki']/value" /><br /><br />
        </div>
        
   <div style="clear:both;"></div>
      <xsl:apply-templates select="document(concat('udata://emarket/order/',order_id))" mode= "unic"/><br /><br /><br />
	<xsl:text>Благодарим Вас за покупку!</xsl:text><br />
    <xsl:text>С уважением, Администрация онлайн магазина principnovo.ru</xsl:text><br />
    <xsl:text> +7 (921) 952-91-90</xsl:text><br />
    <xsl:text>www.principnovo.ru</xsl:text>
	</xsl:template>

	<xsl:template match="neworder_notification">
    <xsl:variable select="document(concat('uobject://',order_id))//property[@name='customer_id']/value/item/@id" name="customer1"/>
   
		<xsl:text>Поступил новый заказ №</xsl:text>
		<xsl:value-of select="order_number" />
		<xsl:text> (</xsl:text>
		<a href="http://{domain}/admin/emarket/order_edit/{order_id}/">
			<xsl:text>Просмотр</xsl:text>
		</a>
		<xsl:text>)</xsl:text><br/><br/>
		<xsl:text>Способ оплаты: </xsl:text>
		<xsl:value-of select="payment_type" /><br/>
		<xsl:text>Статус оплаты: </xsl:text>
		<xsl:value-of select="payment_status" /><br/>
		<xsl:text>Сумма оплаты:  </xsl:text>
		<xsl:value-of select="price" /><br/>
        <xsl:text>Имя:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='fname']/value" /><br />
        <xsl:text>Телефон:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='phone']/value" /><br />
        <xsl:text>E-mail:  </xsl:text>
		<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='email']/value" /><br /><br />
        
        <xsl:text>Желаемое время звонка:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='zhelaemoe_vremya_zvnoka']/value" /><br />
        <xsl:text>Адрес доставки:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='adres_dostavki']/value" /><br /><br />
<xsl:text>Время доставки:  </xsl:text>
<xsl:value-of select="document(concat('uobject://',$customer1))//property[@name='vremya_dostavki']/value" /><br /><br />
		
        
        <xsl:apply-templates select="document(concat('udata://emarket/order/',order_id))" mode= "unic"/>
	</xsl:template>

<xsl:template match="udata" mode= "unic">
<div style="padding:20px; width: 90%;
margin-top: 10px; border-top: 3px solid #964B00; border-left:1px solid #999;  border-right:1px solid #999; border-bottom:1px solid #999;">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <thead>
  <tr>
    <td style="background:#CCC;">Наименование</td>
    <td style="background:#CCC;">Цена</td>
    <td style="background:#CCC;" align="right">Количество</td>
  </tr>
  </thead>
   <tbody>
   <xsl:apply-templates select="items/item" mode = "lin"/>
   </tbody>
   
   
  
    
  <tr>
    <td>
    Всего товаров:
    </td>
    <td></td>
    <td align="right">
    <xsl:value-of select="summary/amount" /> шт.
    </td>
  </tr>
  <tr>
    <td>
    На сумму:
    </td>
    <td></td>
    <td align="right">
    <xsl:value-of select="summary/price/actual" />
    <xsl:value-of select="summary/price/@suffix" />.
    </td>
  </tr>
 
</table>
</div>
</xsl:template>
 
<xsl:template match="udata//item" mode= "lin">
<tr style=" border-bottom:#333 1px dashed;">
  <td style="padding-bottom:10px; padding-top:10px;"><a href="{page/@link}"><xsl:value-of select="@name"/></a></td>
  <td style="padding-bottom:10px; padding-top:10px;"><xsl:value-of select="price"/> <xsl:value-of select="price/@suffix" />.</td>
  <td align="right" style="padding-bottom:10px; padding-top:10px;"><xsl:value-of select="amount"/> шт.</td>
</tr>
</xsl:template>
</xsl:stylesheet>