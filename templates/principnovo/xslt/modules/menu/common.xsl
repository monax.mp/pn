<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://mobile/i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	
	<xsl:template match="udata[@module = 'menu']" mode="bottom_menu_draw">
		<xsl:apply-templates select="item" mode="bottom_menu_draw1" />		
	</xsl:template>

	<xsl:template match="udata[@module = 'menu']/item" mode="bottom_menu_draw1">
		<div href="{@link}" class="footer-cols" style="">
			<a href="{@link}" class="footer-menu-1"><xsl:value-of select="node()" /></a>
			<xsl:apply-templates select="items/item" mode="bottom_menu_draw2" />		
		</div>		
	</xsl:template>
	
	<xsl:template match="items/item" mode="bottom_menu_draw2">		
		<a href="{@link}" class="footer-menu-2"><xsl:value-of select="node()" /></a>
		<xsl:apply-templates select="items/item" mode="bottom_menu_draw3" />		
	</xsl:template>
	
	<xsl:template match="items/item" mode="bottom_menu_draw3">		
		<a href="{@link}" class="footer-menu-3"><xsl:value-of select="node()" /></a>
	</xsl:template>
	
	<!-- Top menu -->
	<xsl:template match="udata[@module = 'menu']" mode="top_menu_draw">
		<!-- <xsl:copy-of select="." /> -->
		<ul umi:element-id="0" umi:module="content" umi:region="list" umi:sortable="sortable" umi:add-method="popup">
        <xsl:apply-templates select="item" mode="top_menu_draw1" />
		</ul>
	</xsl:template>

	<xsl:template match="item" mode="top_menu_draw1">
		<xsl:variable name="coll_k" select="substring-after(node(),'_')" />
		<xsl:variable name="coll_name">
			<xsl:if test="not($coll_k)">
				<xsl:value-of select="node()" />
			</xsl:if>
			<xsl:value-of select="substring-before(node(),'_')" />
		</xsl:variable>
		<li>
			<xsl:choose>
				<xsl:when test="$coll_k = '2'">
					<xsl:attribute name="class">two-col-mens c<xsl:value-of select="count(items/item)" /></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">one-col-mens c<xsl:value-of select="count(items/item)" /></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<!-- <xsl:if test="position() = 2">
				<xsl:attribute name="class">one-col-mens</xsl:attribute>
			</xsl:if>
			<xsl:if test="position() = 4">
				<xsl:attribute name="class">one-col-mens2</xsl:attribute>
			</xsl:if>
			<xsl:if test="position() = 6">
				<xsl:attribute name="id">shop</xsl:attribute>
			</xsl:if> -->
			<a href="{@link}" umi:element-id="{@id}" umi:region="row" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
				<xsl:value-of select="$coll_name" />
			</a>
			<xsl:apply-templates select="items" mode="top_menu_draw2"/>
		</li>
	</xsl:template>
	
    <xsl:template match="item[@status = 'active']" mode="top_menu_draw1">
		<xsl:variable name="coll_k" select="substring-after(node(),'_')" />
		<xsl:variable name="coll_name">
			<xsl:if test="not($coll_k)">
				<xsl:value-of select="node()" />
			</xsl:if>
			<xsl:value-of select="substring-before(node(),'_')" />
		</xsl:variable>
		<li>
			<xsl:choose>
				<xsl:when test="$coll_k = '2'">
					<xsl:attribute name="class">two-col-mens c<xsl:value-of select="count(items/item)" /></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="class">one-col-mens c<xsl:value-of select="count(items/item)" /></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<a class="act" href="{@link}" umi:element-id="{@id}" umi:region="row" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
				<xsl:value-of select="$coll_name" />
			</a>
			<xsl:apply-templates select="items" mode="top_menu_draw2"/>
		</li>
	</xsl:template>
	
    <xsl:template match="items" mode="top_menu_draw2">
		
		<ul class="sub-menu">
			<xsl:apply-templates select="item" mode="top_menu_draw2" />
		</ul>
	</xsl:template>
	<xsl:template match="item" mode="top_menu_draw2">
		<li>
			<a href="{@link}"><xsl:value-of select="node()" /></a>
		</li>
	</xsl:template>
	
	 <!-- right menu -->
	<xsl:template match="item" mode="right_menu_draw">
		<div class="border" style="padding-top:20px; padding-bottom:20px; margin-bottom:20px;">
			<ul class="right-nav">
				<xsl:apply-templates select="items/item" mode="right_menu_draw" />
			</ul>
		</div>	
	</xsl:template>
    
    <xsl:template match="items/item" mode="right_menu_draw">
		<li><a href="{@link}"><xsl:value-of select="node()" /></a></li>
    </xsl:template>
	

	<!-- Header menu -->
	<xsl:template match="udata[@module = 'menu']" mode="oinfo-pages-head">
		<ul>
			<xsl:apply-templates select="item" mode="info-pages-head" />
		</ul>
	</xsl:template>

	<xsl:template match="udata[@module = 'menu']/item" mode="oinfo-pages-head">
		<li>
			<a href="{@link}">
				<xsl:value-of select="node()" />
			</a>
		</li>
	</xsl:template>
	
	<xsl:template match="udata[@module = 'menu']/item[@link = '/help/']" mode="oinfo-pages-head">
		<li>
			<a href="{@link}" class="help">
				<xsl:value-of select="node()" />
			</a>
		</li>
	</xsl:template>
	
</xsl:stylesheet>