<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:template match="/result[@method = 'search_do']">
    <div style=" margin-left:1.4%; margin-right:1.4%;">
		<xsl:apply-templates select="document('udata://search/search_do')" />
    </div>
	</xsl:template>
	
	<xsl:template match="udata[@method = 'search_do']">
		<form class="search" action="/search/search_do/" method="get">
        <input type="hidden" name="search_branches" value="7" />
        <input type="hidden" name="search_types" value="56" />
			<input style="border: 1px solid #ccc;
    border-radius: 5px;
    height: 24px;
    margin-right: 5px;
    padding-left: 5px;
    width: 240px;" type="text" value="{$search_string}" name="search_string" class="textinputs"  x-webkit-speech="" speech="" />
			<input type="submit" class="button" value="&search;" />
		</form>

		<p>
			<strong>
				<xsl:text>&search-founded-left; "</xsl:text>
				<xsl:value-of select="$search_string" />
				<xsl:text>" &search-founded-nothing;.</xsl:text>
			</strong>
		</p>
	</xsl:template>
	
	<xsl:template match="udata[@method = 'search_do' and count(items/item)]">
		<form class="search" action="/search/search_do/" method="get">
        <input type="hidden" name="search_branches" value="7" />
        <input type="hidden" name="search_types" value="56" />
			<input  style="border: 1px solid #ccc;
    border-radius: 5px;
    height: 24px;
    margin-right: 5px;
    padding-left: 5px;
    width: 240px;" type="text" value="{$search_string}" name="search_string" class="textinputs"  x-webkit-speech="" speech="" />
			<input type="submit" class="button" value="&search;" />
		</form>

		<p>
			<strong>
				<xsl:text>&search-founded-left; "</xsl:text>
				<xsl:value-of select="$search_string" />
				<xsl:text>" &search-founded-right;: </xsl:text>
				<xsl:value-of select="total" />
				<xsl:text>.</xsl:text>
			</strong>
		</p>
<div style="height:10px; margin-bottom:10px; border-bottom:1px dotted #333333;"></div>

		<div class="search left-col" style="float:none;width:100%;">
			<!-- <xsl:apply-templates select="items/item" mode="search-result" /> -->
			<xsl:apply-templates select="items/item" mode="short-view" />
            <div style="clear:both;"></div>
		</div>
		<xsl:apply-templates select="total" />
	</xsl:template>
	
	<xsl:template match="item" mode="search-result">
		<div class="sera-res">
		<a style="display:block; overflow:hidden; text-decoration:none; padding-bottom:5px;"  href="{@link}" umi:element-id="{@id}" umi:field-name="name">
		
		<xsl:apply-templates select="document(concat('upage://', @id))/udata" mode="extra_search"/>
        </a>
        <a class="green-button" href="{@link}">
					Купить
					</a>
        </div>
		
			
		
	</xsl:template>
    
    <xsl:template match="udata"  mode="extra_search"> 
    <div class="imges-res" style="text-align:center;">
<xsl:apply-templates select="document(concat('udata://system/makeThumbnail/(',substring(.//property[@name='photo']/value,2),')'))/udata" />
    </div>
    <div style="text-align:left; height:38px; overflow:hidden; margin-bottom:10px; color:#090; margin-top:5px;">	
    <xsl:value-of select=".//property[@name='h1']/value"  disable-output-escaping="yes"/>
	</div>	
    <div style="text-align:left; font-size:18px; color:#333;">
    <xsl:value-of select=".//property[@name='price']/value"  disable-output-escaping="yes"/> р.
    </div>
    
</xsl:template>
</xsl:stylesheet>