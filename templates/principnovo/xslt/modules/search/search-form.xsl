<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="search-form-left-column">
		<form class="search" style="margin:0px;" action="/search/search_do/" method="get">
        <input type="hidden" name="search_branches" value="7" />
        <input type="hidden" name="search_types" value="56 415" />
       <input type="text" value="" name="search_string" class="textinput" style="margin-top:5px; margin-bottom:5px; height:30px; width:100%;"  placeholder="Поиск по каталогу" />
		</form>
	</xsl:template>
</xsl:stylesheet>