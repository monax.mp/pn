<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="/result[@module = 'catalog' and @method = 'object']">
		<xsl:apply-templates select="document(concat('udata://content/addRecentPage/', $document-page-id))/udata" />
		<xsl:apply-templates select="document(concat('upage://', page/@id,'?show-empty'))/udata" mode="object-view" />
	</xsl:template>

	<xsl:template match="udata" mode="object-view">
		<xsl:variable name="cart_items" select="document('udata://emarket/cart/')/udata/items" />
        <xsl:variable name="new" select=".//property[@name = 'novinki_ili_akcii']/value/item/@id"/>
        
     <div class="object">
     <div class="box50l"><!--<a class="fen" href="{.//property[@name = 'photo']/value}"><img src="{.//property[@name = 'photo']/value}"/></a>-->
<script type="text/javascript">
function l_image (a) {
 document.example_img.src=a
}
</script>
     <div id="big" style="position:relative;">
     <div style="position:absolute; z-index:500; width:60px; height:60px; left:0px; top:0px;">
     <xsl:if test="$new = 2339">
     <img src="/templates/principnovo/img/new.png" width="60" height="60" style="width:100%; height:auto; border:none;" />
     </xsl:if>
     </div>
     <img name="example_img" src="{.//property[@name = 'photo']/value}"/>
     </div>
     <!--<div id="preview"> 
    <a href="javascript:l_image ('{.//property[@name = 'photo']/value}')"> <img class="priviv" src="{.//property[@name = 'photo']/value}"/></a>
     <a href="javascript:l_image ('/images/cms/data/import_files/ee/vishnya_latvian_matala.jpg')"><img class="priviv" src="/images/cms/data/import_files/ee/vishnya_latvian_matala.jpg"/></a>
     <a href="javascript:l_image ('/images/cms/data/import_files/ee/vishnya_mustilan_morelli.jpg')"><img class="priviv" src="/images/cms/data/import_files/ee/vishnya_mustilan_morelli.jpg"/></a>
     </div>-->
     <div class="clear"></div>
     </div>
     <div class="box50"><xsl:apply-templates select=".//property[@name = '&property-description;']" /></div>
     <div class="clear"></div>
 

	<xsl:choose>
		<xsl:when test=".//group[@name = 'catalog_option_props']/property/value/option">
			<form id="add_basket_{page/@id}" class="options" action="{$lang-prefix}/emarket/basket/put/element/{page/@id}/">
				<xsl:apply-templates select=".//group[@name = 'catalog_option_props']/property" mode="newtable_options">
					<xsl:with-param name="cart_items" select="$cart_items" />
				</xsl:apply-templates>
			</form>
		</xsl:when>
		<xsl:otherwise>
<table width="100%" cellpadding="5" border="0" cellspacing="0" style="margin-top:20px; margin-bottom:40px; border-bottom:1px solid #999;">
 <thead>
  <tr>
    <td align="center" style="border-bottom:1px solid #999;">Размер</td>
    <!--<td align="center" style="border-bottom:1px solid #999;">На складе</td>-->
    <td align="center" style="border-bottom:1px solid #999;">Цена</td>
    <td align="center" style="border-bottom:1px solid #999;">Количество</td>
    <td style="color:#FFF;border-bottom:1px solid #999;">.</td>
  </tr>
  </thead>
  <tbody>
   <tr>
    <td>
    <xsl:apply-templates select=".//property[@name = 'kontejner']" />
    <xsl:text>&#160;</xsl:text>
    <xsl:apply-templates select=".//property[@name = 'vysota']" />
    </td>
    <!--<td align="center"><xsl:apply-templates select=".//property[@name = 'common_quantity']" /> шт</td>-->
    <td align="center">
    <div class="price" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
						<span umi:element-id="{page/@id}" umi:field-name="price" itemprop="price">
							<!-- <xsl:apply-templates select="document(concat('udata://emarket/price/', page/@id))" /> -->
							По запросу
						</span>
						<link itemprop="itemCondition" href="http://schema.org/NewCondition" />
					</div>
    </td>
    <td align="center">
    <input type="input" style="padding:8px; text-align:center;" value="1" size="3" id="amount_{page/@id}"/>
    </td>
    <td align="center">
    <!-- -->
			<a href="#zakaz" id="add_zakaz_{$document-page-id}" class="button big red-button fen" style="font-size:13px;" data-page="{$document-page-id}">
				<xsl:text>Заказать</xsl:text>
			</a>         
         </td>
  </tr>
  <!-- <xsl:apply-templates select=".//property[@name = 'gabarity']/value" mode="razmery-view" />   -->
  </tbody>
</table>
		</xsl:otherwise>
	</xsl:choose>
	
<div class="recomareder">
<p style="font-size:17px;border-bottom: 1px solid #666;padding: 0 0 10px;">С ЭТИМ ТОВАРОМ ПОКУПАЮТ</p>
<style>
.container .object .box50 {
   
    margin-left:0px !important;
   
}
</style>
<xsl:apply-templates select=".//property[@name = 's_etim_pokupayut']/value" mode="short-view" />
</div>
     </div>
	
	<div style="display:none">
		<xsl:variable name="webform" select="document('udata://webforms/add/140/')/udata" />
		<div id="zakaz" style="width:300px;">
			<form method="post" action="/webforms/send/">
				<input type="hidden" name="system_email_to" value="{$webform/items/item[@selected='selected']/@id}" />
				<input type="hidden" name="system_form_id" value="{$webform/@form_id}" />
				<input type="hidden" name="system_template" value="webforms" />
				<!-- <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{$webform/@form_id}/" /> -->
				<h3>Растения на заказ</h3>
				<legend></legend>
				<xsl:apply-templates select="$webform/groups/group/field" mode="webforms-zakaz" />
				<input type="submit" value="Заказать" class="button big green-button" />
			</form>
		</div>
    </div>
    </xsl:template>

	<xsl:template match="property[@name = '&property-description;']">
		<div class="descr" itemprop="description">
			<div umi:element-id="{../../../@id}" umi:field-name="{@name}" umi:empty="&item-description;">
				<xsl:value-of select="value" disable-output-escaping="yes" />
			</div>
		</div>
	</xsl:template>
	
	<xsl:template match="property[@name = '&property-description;' and value = '']">
		<div class="descr" itemprop="description">
			<div umi:element-id="{../../../@id}" umi:field-name="{@name}" umi:empty="&item-description;">
				
			</div>
		</div>
	</xsl:template>
	
	<xsl:template match="group" mode="table">
		<table class="object">
			<thead>
				<tr>
					<th colspan="2">
						<xsl:value-of select="concat(title, ':')" />
					</th>
				</tr>
			</thead>
			<tbody umi:element-id="{../../@id}">
				<xsl:apply-templates select="property" mode="table" />
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="property" mode="table">
		<tr>
			<xsl:apply-templates select="title" mode="table"/>
			<xsl:apply-templates select="value" mode="table"/>
		</tr>
	</xsl:template>
	<xsl:template match="property/title" mode="table">
		<td>
			<span>
				<xsl:apply-templates select="document(concat('utype://', ../../../../@type-id, '.', ../../@name))/udata/group/field[@name = ../@name]/tip" mode="tip" />
				<xsl:value-of select="." />
			</span>
		</td>
	</xsl:template>
	<xsl:template match="property/value" mode="table">
		<td umi:field-name="{../@name}">
			<xsl:apply-templates select=".." />
		</td>
	</xsl:template>
	<xsl:template match="property[@type='symlink']/value" mode="table">
		<td umi:field-name="{../@name}" umi:type="catalog::object">
			<xsl:apply-templates select=".." />
		</td>
	</xsl:template>
	<xsl:template match="property[@type='wysiwyg']/value" mode="table">
		<td umi:field-name="{../@name}">
			<xsl:value-of select="." disable-output-escaping="yes" />
		</td>
	</xsl:template>
	<xsl:template match="group" mode="div">
		<div class="item_properties">
			<div>
				<xsl:value-of select="concat(title, ':')" />
			</div>
			<xsl:apply-templates select="property" mode="div" />
		</div>
	</xsl:template>

	<xsl:template match="property" mode="div">
		<xsl:apply-templates select="document(concat('utype://', ../../../@type-id, '.', ../@name))/udata/group/field[@name = ./@name]/tip" mode="tip" />
		<xsl:value-of select="title" />
		<xsl:text>: </xsl:text>
		<span umi:field-name="{@name}"><xsl:apply-templates select="." /></span>
		<xsl:text>; </xsl:text>
	</xsl:template>
	
	<xsl:template match="property[last()]" mode="div">
		<xsl:apply-templates select="document(concat('utype://', ../../../@type-id, '.', ../@name))/udata/group/field[@name = ./@name]/tip" mode="tip" />
		<xsl:value-of select="title" />
		<xsl:text>: </xsl:text>
		<span umi:field-name="{@name}"><xsl:apply-templates select="." /></span>
		<xsl:text>. </xsl:text>
	</xsl:template>
	
	<xsl:template match="property[@name = 'udachno_sochetaetsya_s']" mode="div">
		<xsl:apply-templates select="document(concat('utype://', ../../../@type-id, '.', ../@name))/udata/group/field[@name = ./@name]/tip" mode="tip" />
		<xsl:value-of select="title" />
		<xsl:text>: </xsl:text>
		<span umi:type="catalog::object" umi:field-name="{@name}"><xsl:apply-templates select="value/page" mode="div" /></span>
		<xsl:text>; </xsl:text>
	</xsl:template>
	
	<xsl:template match="property[@name = 'udachno_sochetaetsya_s' and last()]" mode="div">
		<xsl:apply-templates select="document(concat('utype://', ../../../@type-id, '.', ../@name))/udata/group/field[@name = ./@name]/tip" mode="tip" />
		<xsl:value-of select="title" />
		<xsl:text>: </xsl:text>
		<span umi:type="catalog::object" umi:field-name="{@name}"><xsl:apply-templates select="value/page" mode="div" /></span>
		<xsl:text>. </xsl:text>
	</xsl:template>
	
	<xsl:template match="page" mode="div">	
		<a href="{@link}">
			<xsl:value-of select="name" />
		</a>
		<xsl:text>, </xsl:text>
	</xsl:template>
	
	<xsl:template match="page[last()]" mode="div">	
		<a href="{@link}">
			<xsl:value-of select="name" />
		</a>
	</xsl:template>
	
	<xsl:template match="group" mode="table_options">
		<xsl:if test="count(//option) &gt; 0">
			<h4><xsl:value-of select="concat(title, ':')" /></h4>
			<xsl:apply-templates select="property" mode="table_options" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="property" mode="table_options">
		<table class="object">
			<thead>
				<tr>
					<th colspan="3">
						<xsl:value-of select="concat(title, ':')" />
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:apply-templates select="value/option" mode="table_options" />
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="option" mode="table_options">
		<tr>
			<td style="width:20px;">
				<input type="radio" name="options[{../../@name}]" value="{object/@id}">
					<xsl:if test="position() = 1">
						<xsl:attribute name="checked">
							<xsl:text>checked</xsl:text>
						</xsl:attribute>
					</xsl:if>
				</input>
			</td>
			<td>
				<xsl:value-of select="object/@name" />
			</td>
			<td align="right">
				<xsl:value-of select="@float" />
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="tip" mode="tip">
		<xsl:attribute name="title">
			<xsl:apply-templates />
		</xsl:attribute>
		<xsl:attribute name="style">
			<xsl:text>border-bottom:1px dashed; cursor:help;</xsl:text>
		</xsl:attribute>
	</xsl:template>

	<xsl:template match="property" mode="newtable_options" />
	<xsl:template match="property[value/option]" mode="newtable_options">
		<xsl:param name="cart_items" />
		<table width="100%" cellpadding="5" border="0" cellspacing="0" style="margin-top:20px; margin-bottom:40px; border-bottom:1px solid #999;">
		 <thead>
			<tr>
				<td align="center" style="border-bottom:1px solid #999;">Размер</td>
				<!--<td align="center" style="border-bottom:1px solid #999;">На складе</td>-->
				<td align="center" style="border-bottom:1px solid #999;">Цена</td>
				<td align="center" style="border-bottom:1px solid #999;">Количество</td>
				<td style="color:#FFF;border-bottom:1px solid #999;">.</td>
			</tr>
		  </thead>
		  
		  <tbody>
			<xsl:apply-templates select="value/option" mode="newtable_options">
				<xsl:sort select="@float" data-type="number" />
				<xsl:with-param name="cart_items" select="$cart_items" />
			</xsl:apply-templates>
		  </tbody>
		</table>
		<!-- <xsl:copy-of select="$cart_items" /> -->
	</xsl:template>

	<xsl:template match="option" mode="newtable_options">
		<xsl:param name="cart_items" />
		<tr>
			<td>
				<xsl:value-of select="object/@name" />
			</td>
			<!--<td align="center"><xsl:value-of select="@int" /> шт</td>-->
			<td align="center">
				<div class="price" itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
					<span itemprop="price">
						<xsl:choose>
							<xsl:when test="@int and @int &gt; 0 and @float &gt; 1">
								<xsl:apply-templates select="." mode="newtable_option_price" />
							</xsl:when>
							<xsl:otherwise>
								По запросу
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<link itemprop="itemCondition" href="http://schema.org/NewCondition" />
				</div>
			</td>
			<td align="center">
				<input type="input" style="padding:8px; text-align:center;" value="1" size="3" id="amount_{$document-page-id}_{object/@id}"/>
			</td>
			
			<td align="center">
				<xsl:choose>
					<xsl:when test="@int and @int &gt; 0 and @float &gt; 1">
						<a href="{$lang-prefix}/emarket/basket/put/element/{$document-page-id}/?options[{../../@name}]={object/@id}" id="add_basket_{$document-page-id}_{object/@id}" class="button big green-button" style="font-size:13px;" data-page="{$document-page-id}" data-options="{../../@name}" data-option="{object/@id}">
							<xsl:text>&basket-add;</xsl:text>
							<xsl:if test="$cart_items/item[options/option/@id = current()/object/@id]">
								<xsl:text> (</xsl:text>
								<xsl:value-of select="sum($cart_items/item[options/option/@id = current()/object/@id]/amount)" />
								<xsl:text>)</xsl:text>
							</xsl:if>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<a href="#zakaz" id="add_zakaz_{$document-page-id}_{object/@id}" class="button big red-button fen" style="font-size:13px;" data-page="{$document-page-id}" data-options="{../../@name}" data-option="{object/@id}">
							<xsl:text>Заказать</xsl:text>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			<!-- -->
		   <!-- <form id="add_basket_{page/@id}" class="options" style="margin:0px;" action="{$lang-prefix}/emarket/basket/put/element/{page/@id}/"> -->
				<!-- <label for="option_{object/@id}">
				<input type="submit" class="button big  green-button" id="add_basket_{$document-page-id}">
					<xsl:attribute name="value">
						<xsl:text>&basket-add;</xsl:text>
						<xsl:if test="$cart_items/item[page/@id = $document-page-id]">
							<xsl:text> (</xsl:text>
							<xsl:value-of select="sum($cart_items/item[page/@id = $document-page-id]/amount)" />
							<xsl:text>)</xsl:text>
						</xsl:if>
					</xsl:attribute>
				</input>
				</label> -->
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match="option" mode="newtable_option_price" />
	<xsl:template match="option[@float &gt; 1]" mode="newtable_option_price">
		<xsl:value-of select="@float" /> р.
	</xsl:template>

	<xsl:template match="field" mode="webforms-zakaz">
		<div>
			<input type="text" name="{@input_name}" style="width:100%;height:30px;margin-bottom:10px;">
				<xsl:if test="@required = 'required'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="placeholder"><xsl:value-of select="@title" /></xsl:attribute>
			</input>
		</div>
	</xsl:template>

	<xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="webforms-zakaz">
		<div>
			<textarea name="{@input_name}" rows="7" placeholder="Сообщение" style="width:100%;height:100px;margin-bottom:10px;">
				<xsl:if test="@required = 'required'">
					<xsl:attribute name="required">required</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="placeholder"><xsl:value-of select="@title" /></xsl:attribute>
			</textarea>
		</div>
	</xsl:template>

</xsl:stylesheet>