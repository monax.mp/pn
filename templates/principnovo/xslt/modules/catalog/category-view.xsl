<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="/result[@method = 'category']">
     <div style="    border: 2px solid #82b064;
    padding: 10px;
    text-align: center;
    margin-bottom: 30px;
    font-size: 16px;
    background: #ececfb;">
<!-- 	border: 1px solid #ccc;
    padding: 10px;
    text-align: center;
    margin-bottom: 30px; -->
<strong>Уважаемые покупатели!</strong><br />
<p>Рады сообщить Вам, что в нашем садовом центре поступление новых растений!<br />
Хвойные и лиственные породы деревьев, многолетние и однолетние цветочные культуры, декоративные и плодово-ягодные кустарники.
Широкий ассортимент растений для вашего сада!</p>

<p>Цены и всю интересующую Вас информацию Вы можете узнать по телефонам:<br />
<strong>+7(921) 952-91-90<br />
+7(812) 702-92-93</strong><br />
Заказы отправляйте на почту: vib.shosse@mail.ru</p>
    </div>
    
    <xsl:apply-templates select="document('udata://content/menu/0/2/(shop)/?extProps=menu_pic_ua,main_list_name')/udata" mode="catalog_menu" />
 
   
    <div class="clear" />
    <h1>Новинки</h1>
    <xsl:apply-templates select="document('usel://special-offers/?limit=9')" mode="special-offers" />
    <div class="clear" />
<div><!--  style="height:1px; overflow:hidden;" -->
<xsl:variable name="descr_razd" select="document(concat('upage://', $document-page-id,'.descr'))/udata//value"/>
<xsl:value-of select="$descr_razd" disable-output-escaping="yes" />
</div>
    </xsl:template>

	<xsl:template match="/result[@method = 'category'][count(/result/parents/page) = 1]">
    
		<xsl:value-of select="document('udata://catalog/setOrderBy/name/')/udata" />
		<xsl:apply-templates select="document(concat('udata://catalog/getObjectsListCustom/', page/@id, '////2/h1/1'))/udata" />
    </xsl:template>

	<xsl:template match="/result[@method = 'category'][count(/result/parents/page) &gt; 1]">
    
		<xsl:value-of select="document('udata://catalog/setOrderBy/name/')/udata" />
		<xsl:apply-templates select="document('udata://catalog/getObjectsListCustom')" />
    </xsl:template>
    
    

	<xsl:template match="udata[@method = 'getObjectsListCustom']">
		<div class="catalog" umi:element-id="{category_id}" umi:module="catalog" umi:method="getObjectsListCustom" umi:sortable="sortable" umi:add-method="popup">
			<xsl:text>&empty-category;</xsl:text>
		</div>
	</xsl:template>
	
	<xsl:template match="udata[@method = 'getObjectsListCustom'][total]">
	<!-- <xsl:copy-of select="." /> -->
<!-- задаем переменную "type" и присваиваем ей значение идентификатора типа данных товаров 
<xsl:variable name="type" select="document(concat('udata://catalog/getObjectsList/', page/@id, '////3/'))/udata/type_id"/>-->
<!-- передаем в catalog/search созданную переменную "type" 
<xsl:apply-templates select="document(concat('udata://catalog/search////', $type))"  mode="top-filtr" />
<div style="padding:10px; background:#82b064; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2) inset; color:#fff; font-size:14px; margin-left: 1%; margin-right: 1%; margin-bottom:20px;">
<form class="catalog_filter">
<label>
<span style="display:block;"><input type="checkbox" onchange="this.form.submit()" name="fields_filter[vlazhnost][]" value="1625"/>Влаголюбивые</span><span style="display:block;"><input type="checkbox" onchange="this.form.submit()" name="fields_filter[vlazhnost][]" value="1626"/>Засухоустойчивые</span><span style="display:block;"><input type="checkbox" onchange="this.form.submit()" name="fields_filter[vlazhnost][]" value="1627"/>Обычные</span></label>
</form>
</div>-->
	<xsl:if test="category_id != '2051'">
<div style="
    background:#82b064;
    border:none;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.2) inset;
    color: #fff;
    padding: 8px 20px;
    text-decoration: none;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
    margin:1%;
    margin-bottom:20px;
    ">
    <style>
    #top_fltr input { display:none;}
	.product-filtr { float:left; margin-left:1%; margin-right:1%; width:23%;}
	.product-filtr-border { border-left:1px dotted #CCCCCC; padding-left:15px;}
	.product-filtr-border label { cursor:pointer;}
	.product-filtr-border label:hover { color:#333;}
	
    </style>
<form action="" id="top_fltr">
					<div class="product-filtr">
						<div style="font-size:18px;">показывать<br/> растения:</div>
					</div>
                    
					<div class="product-filtr">
					  <div class="product-filtr-border">
					    <div>
					      <input type="checkbox" id="water1" name="fields_filter[vlazhnost][]" value="1625" onchange="this.form.submit()"/>
					      <label for="water1"><span class="ico ico8"></span>влаголюбивые</label>
				        </div>
					    <div>
					      <input type="checkbox" id="water2" name="fields_filter[vlazhnost][]" value="1626" onchange="this.form.submit()"/>
					      <label for="water2"><span class="ico ico9"></span>обычные</label>
				        </div>
					    <div>
					      <input type="checkbox" id="water3" name="fields_filter[vlazhnost][]" value="1627" onchange="this.form.submit()"/>
					      <label for="water3"><span class="ico ico10"></span>засухоустойчивые</label>
				        </div>
				      </div>
					</div>
					<div class="product-filtr">
					  <div class="product-filtr-border">
					    <div>
					      <input type="checkbox" id="sun1" name="fields_filter[svetolyubie][]" value="2215" onchange="this.form.submit()"/>
					      <label for="sun1"><span class="ico ico11"></span>светолюбивые</label>
				        </div>
					    <div>
					      <input type="checkbox" id="sun2" name="fields_filter[svetolyubie][]" value="2213" onchange="this.form.submit()"/>
					      <label for="sun2"><span class="ico ico12"></span>теневыносливые</label>
				        </div>
					    <div><br/>
					     
				        </div>
				      </div>
					</div>
                    <div class="product-filtr">
                      <div class="product-filtr-border" style="font-size:15px;font-weight:bold;">
                        <div>
                        <input type="checkbox" id="new1" name="fields_filter[novinki_ili_akcii][]" value="2339" onchange="this.form.submit()"/>
                          <label for="new1"><span class="ico ico8"></span>новинки</label>
                        </div>
                        <div>
                        <input type="checkbox" id="sklad1" name="fields_filter[common_quantity][]" value="1" onchange="this.form.submit()"/>
                          <label for="sklad1"><span class="ico ico8"></span>в наличии</label>
                        </div>
                        <div>
                        <br />
                         <!-- <input type="checkbox" id="new2" name="fields_filter[novinki_ili_akcii][]" value="1" onchange="this.form.submit()"/>
                          <label for="new2"><span class="ico ico9"></span>акции</label>-->
                        </div>
                        <div>
                          <br /> <!--<input type="checkbox" id="new3" name="fields_filter[novinki_ili_akcii][]" value="2214" onchange="this.form.submit()"/>
                          <label for="new3"><span class="ico ico10"></span>скоро в продаже</label>-->
                        </div>
                      </div>
					</div>
				</form>
<div class="clear" />  
</div>
</xsl:if>
      <div class="catalog_sort" />
		<div class="catalog">
		  <div class="objects" umi:element-id="{category_id}" umi:module="catalog" umi:method="getObjectsListCustom" umi:sortable="sortable">
				<xsl:apply-templates select="lines/item" mode="short-view">
					<xsl:with-param name="cart_items" select="document('udata://emarket/cart/')/udata/items" />
				</xsl:apply-templates>
				<div class="clear" />
			</div>
		</div>
		<div class="clear" />
		
		<xsl:apply-templates select="total" />



<xsl:if test="not($p) or $p = 0">
	<div id="text1"><!--  style=" height:104px; overflow:hidden;" -->    
	<xsl:variable name="descr_razd" select="document(concat('upage://', $document-page-id,'.descr'))/udata//value"/>
	<xsl:value-of select="$descr_razd" disable-output-escaping="yes" />
	</div>
</xsl:if>
<!-- <span id="ontext1" style="display: inline;">
<a href="javascript:on('1');" style="color:#060;">Развернуть</a>
</span>  
<span id="offtext1" style="display: none;">
<a href="javascript:off('1');"  style="color:#060;">Свернуть</a>
</span> -->
<!-- <script>
function on(n){  
     eval("document.all.text"+n+".style.height='auto';");  
     eval("document.all.ontext"+n+".style.display='none';");  
     eval("document.all.offtext"+n+".style.display='inline';");}  
function off(n){  
     eval("document.all.text"+n+".style.height='104px';");  
     eval("document.all.ontext"+n+".style.display='inline';");  
     eval("document.all.offtext"+n+".style.display='none';");} 
</script>	   -->
	</xsl:template>
</xsl:stylesheet>