<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">


	<xsl:include href="category-list.xsl" />
	<xsl:include href="object-view.xsl" />
	<xsl:include href="category-view.xsl" />
	
	<xsl:include href="search-filter.xsl" />
	<xsl:include href="recommended.xsl" />
	<xsl:include href="left-column-category-list.xsl" />
	<xsl:include href="special-offers.xsl" />

	<xsl:template match="page|item" mode="short-view">
		<xsl:param name="from_recent" select="false()" />
		<xsl:param name="cart_items" select="false()" />
		<xsl:param name="spesial-view" select="false()" />
<xsl:variable name="object" select="document(concat('upage://', @id))/udata" />
<xsl:variable name="cont" select="document(concat('upage://', @id,'.kontejner'))/udata/property/value/item/@name"/>
<xsl:variable name="visot" select="document(concat('upage://', @id,'.vysota'))/udata/property/value/item/@name"/>
<xsl:variable name="new" select="document(concat('upage://', @id,'.novinki_ili_akcii'))/udata/property/value/item/@id"/>
<!-- <xsl:variable name="sklad" select="document(concat('upage://', @id,'.stores_state'))/value/option/@int"/> -->
<xsl:variable name="sklad" select="$object//property[@name='common_quantity']/value"/>
        <xsl:variable name="is_options">
			<xsl:apply-templates select="$object/page/properties" mode="is_options" />
		</xsl:variable>
		<xsl:variable name="price" select="document(concat('udata://emarket/price/', @id))" />
		<xsl:variable name="options" select="$object//group[@name = 'catalog_option_props']/property/value/option" />
		<div class="product" umi:region="row" umi:element-id="{@id}">
			<xsl:if test="$spesial-view=true()">
				<xsl:attribute name="class">product2</xsl:attribute>
			</xsl:if>
            <a href="{@link}" class="image">
            <div class="box50l" style="position:relative;">
            <div style="position:absolute; z-index:500; width:60px; height:60px; left:0px; top:0px;">
			<!-- <xsl:copy-of select="$object"/> -->
           <xsl:if test="$new = 2339">
           <img src="/templates/principnovo/img/new.png" width="60" height="60" />
           </xsl:if>
           </div>
				<!-- <img src="{$object//property[@name='photo']/value}" /> -->
               <xsl:call-template name="catalog-thumbnail">
					<xsl:with-param name="element-id" select="@id" />
					<xsl:with-param name="field-name">photo</xsl:with-param>
					<xsl:with-param name="empty">&empty-photo;</xsl:with-param>
					<xsl:with-param name="width">200</xsl:with-param>
                    <xsl:with-param name="height">150</xsl:with-param>
				</xsl:call-template>
			</div>
            </a>
            <div class="box50"> 
            <div>
            <a href="{@link}" class="title" umi:element-id="{@id}" umi:field-name="name" umi:delete="delete">
            <h3 style="height:36px;"><xsl:value-of select="name | @name" disable-output-escaping="yes" /><xsl:if test="not(name | @name)"><xsl:apply-templates /></xsl:if></h3>
            </a>
            <div style="background:url(/templates/principnovo/36810.png) left center no-repeat; background-size:contain; padding-left:25px; height:16px;">
				<span style="font-size:14px; font-weight:400;">
					<xsl:choose>
						<xsl:when test="$price/udata/price/actual or not($options)">
							<xsl:value-of select="$visot" />
							<xsl:text>&#160;</xsl:text>
							<xsl:value-of select="$cont" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="not($options[@float &gt; 1])">
									<xsl:value-of select="$options[1]/object/@name" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:apply-templates select="$options[@float &gt; 1][@int &gt; 0]" mode="option_name">
										<xsl:sort select="@float" data-type="number" order="ascending"/>
									</xsl:apply-templates>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
            <!--<div><stdong>Свет:</stdong> Светолюбивое</div>
            <div><stdong>Влажность:</stdong> Обычная</div>-->
            <div class="price" style="margin-top:10px;">
				<span class="qyn"><!--  umi:element-id="{@id}" umi:field-name="price" -->
					<strong style="font-size:18px;">
						<xsl:choose>
							<!-- <xsl:when test="$price/udata/price/actual">
								<xsl:apply-templates select="$price" />
							</xsl:when> -->
							<xsl:when test="$options[@float &gt; 1]">
								<xsl:apply-templates select="$options[@float &gt; 1][@int &gt; 0]" mode="option_price">
									<xsl:sort select="@float" data-type="number" order="ascending"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:otherwise>
								По запросу
							</xsl:otherwise>
						</xsl:choose>
					</strong>
				</span>
			</div>
			
        </div>
       </div>
       <div class="clear"></div>

		<div class="buttons" style="margin-top:10px;">
		<!-- <xsl:if test="not(../option[@float &gt; 1])"> -->
			<xsl:choose>
				<!-- <xsl:when test="$sklad &gt; 0 and not($options)">
					<div class="add_from_list">
						<a class="green-button qyn" href="{@link}">
						Купить
						</a>
					</div>
				</xsl:when> -->
				<xsl:when test="$sklad &gt; 0 and $options[@float &gt; 1 and @int &gt; 0]">
					<div class="add_from_list">
						<a class="green-button qyn" href="{@link}">
						Купить
						</a>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="add_from_list">
						<a class="green-button qyn" style="background:#F00;" href="{@link}">
						под заказ
						</a>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
   </div>
		<xsl:if test="position() mod 3 = 0">
			<div style="display:block; clear:both; margin-top:10px; margin-bottom:10px; height:1px; background:#CCC;"></div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="properties" mode="is_options">
		<xsl:value-of select="false()" />
	</xsl:template>

	<xsl:template match="properties[group[@name = 'catalog_option_props']/property]" mode="is_options">
<xsl:value-of select="true()" />
	</xsl:template>
    
    <xsl:template match="page|item" mode="rec-view">
		<xsl:param name="from_recent" select="false()" />
		<xsl:param name="cart_items" select="false()" />
		<xsl:variable name="object" select="document(concat('upage://', @id))/udata" />
		<xsl:variable name="is_options">
			<xsl:apply-templates select="$object/page/properties" mode="is_options" />
		</xsl:variable>
       
		<div class="product" umi:region="row" umi:element-id="{@id}" style="margin-bottom:20px;">
			<a href="{@link}" class="image">
            <div style="text-align:center;">
               <xsl:call-template name="catalog-thumbnail">
					<xsl:with-param name="element-id" select="@id" />
					<xsl:with-param name="field-name">photo</xsl:with-param>
					<xsl:with-param name="empty">&empty-photo;</xsl:with-param>
					<xsl:with-param name="width">200</xsl:with-param>
                </xsl:call-template>
			</div>
            </a>
            <div> 
            <div style="text-align:center;">
            <a href="{@link}" class="title" style="color:#690; text-decoration:none; font-size:14px;" umi:element-id="{@id}" umi:field-name="name" umi:delete="delete">
            <xsl:value-of select="name" /><xsl:if test="not(name)"><xsl:apply-templates /></xsl:if>
            </a>
            <!--<div><stdong>Размер:</stdong> 1,5 - 3.0 м</div>
            <div><stdong>Свет:</stdong> Светолюбивое</div>
            <div><stdong>Влажность:</stdong> Обычная</div>
            <div class="price" style="margin-top:10px;">
				<span umi:element-id="{@id}" umi:field-name="price">
					<stdong style="font-size:18px;"><xsl:apply-templates select="document(concat('udata://emarket/price/', @id))" /></stdong>
				</span>
			</div>-->
			
        </div>
       </div>
       <div class="clear"></div>
       <!--<div class="buttons" style="margin-top:10px;">
				<div class="add_from_list">
					<a class="green-button" href="{@link}">
					Купить
					</a>
				</div>
			</div>-->
       </div>
		
	</xsl:template>
    
	<!-- OLD -->
    <xsl:template match="page|item" mode="razmery-view">
		<xsl:param name="from_recent" select="false()" />
		<xsl:param name="cart_items" select="false()" />
		<xsl:variable name="object" select="document(concat('upage://', @id))/udata" />
	    <xsl:variable name="sklad" select="document(concat('upage://', @id, '.common_quantity'))//value"/>
<xsl:variable name="cont" select="document(concat('upage://', @id,'.kontejner'))/udata/property/value/item/@name"/>
<xsl:variable name="visot" select="document(concat('upage://', @id,'.vysota'))/udata/property/value/item/@name"/>
    <tr>
       <td style="border-top:1px solid #999;">
       <xsl:value-of select="$cont" /><xsl:text>&#160;</xsl:text><xsl:value-of select="$visot" />
       </td>
       <!--<td align="center" style="border-top:1px solid #999;">
<xsl:choose>
<xsl:when test="$sklad &gt; 0">
<xsl:value-of select="document(concat('upage://', @id, '.common_quantity'))//value" disable-output-escaping="yes" /> шт  </xsl:when>
	<xsl:otherwise>
	<span style="color:#900;">Нет на складе</span>
	</xsl:otherwise>
</xsl:choose>
       
       </td> -->
       <td align="center" style="border-top:1px solid #999;">
       <span umi:element-id="{@id}" umi:field-name="price">
	   <xsl:apply-templates select="document(concat('udata://emarket/price/', @id))" />
	   </span>
	   </td>
       <td align="center" style="border-top:1px solid #999;">
       <input type="input" style="padding:8px; text-align:center;" value="1" size="3" id="amount_{@id}"/>
       </td>
       <td align="center" style="border-top:1px solid #999;">
       

      
    <div class="add_from_list">
		<a id="add_basket_{@id}"
		class="green-button basket_list" href="{$lang-prefix}/emarket/basket/put/element/{@id}/">
		<xsl:text>&basket-add; </xsl:text>	
		<xsl:variable name="element_id" select="@id" />
		<xsl:if test="$cart_items and $cart_items/item[page/@id = $element_id]">
			<xsl:text> (</xsl:text>
			<xsl:value-of select="sum($cart_items/item[page/@id = $element_id]/amount)" />
			<xsl:text>)</xsl:text>
		</xsl:if>
		</a>
      </div>  

       
					
		
       </td>
	</tr>	
	</xsl:template>




<xsl:template match="page|item" mode="spesial-view">
		<xsl:param name="from_recent" select="false()" />
		<xsl:param name="cart_items" select="false()" />
        <li class="product" umi:region="row" umi:element-id="{@id}" style="margin-left:0px; margin-right:5px;">
			<xsl:apply-templates select="." mode="short-view">
				<xsl:with-param name="spesial-view" select="true()" />
				<xsl:with-param name="cart_items" select="$cart_items" />
			</xsl:apply-templates>
		</li>
	</xsl:template>
	
	<!-- <xsl:template match="group[@name = 'catalog_option_props']/property/value/option" mode="option_name" />
	<xsl:template match="group[@name = 'catalog_option_props']/property/value/option[1]" mode="option_name">
		<xsl:if test="not(../option[@float &gt; 1])">
			<xsl:value-of select="object/@name" />
		</xsl:if>
	</xsl:template> -->
	<xsl:template match="group[@name = 'catalog_option_props']/property/value/option" mode="option_name">
		<xsl:if test="position() = 1">
			<xsl:value-of select="object/@name" />
		</xsl:if>
	</xsl:template>
	
	<!-- <xsl:template match="group[@name = 'catalog_option_props']/property/value/option" mode="option_price" /> -->
	<xsl:template match="group[@name = 'catalog_option_props']/property/value/option" mode="option_price">
		<xsl:if test="position() = 1">
			<xsl:value-of select="@float" /> р.
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
