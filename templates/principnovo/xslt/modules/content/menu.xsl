<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">


	
	<!-- Top menu -->
	<xsl:template match="udata[@method = 'menu']" mode="top_menu">
		<ul umi:element-id="0" umi:module="content" umi:region="list" umi:sortable="sortable" umi:add-method="popup">
        <xsl:apply-templates select="items/item" mode="top_menu" />
		</ul>
	</xsl:template>

	<xsl:template match="item" mode="top_menu">
       <li>
       <xsl:if test="position() = 2">
				<xsl:attribute name="class">one-col-mens</xsl:attribute>
	   </xsl:if>
       <xsl:if test="position() = 4">
				<xsl:attribute name="class">one-col-mens2</xsl:attribute>
	   </xsl:if>
       <xsl:if test="position() = 6">
				<xsl:attribute name="id">shop</xsl:attribute>
	   </xsl:if>
<a href="{@link}" umi:element-id="{@id}" umi:region="row" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
			<xsl:value-of select="." />
      </a>
<xsl:apply-templates select="document(concat('udata://content/menu/0/1/', @id))/udata[items/item]" mode="sub_menu"/>
       </li>
	</xsl:template>
    <xsl:template match="item[@status = 'active']" mode="top_menu">
     <li>
       <xsl:if test="position() = 2">
				<xsl:attribute name="class">one-col-mens</xsl:attribute>
	   </xsl:if>
       <xsl:if test="position() = 4">
				<xsl:attribute name="class">one-col-mens2</xsl:attribute>
	   </xsl:if>
       <xsl:if test="position() = 6">
				<xsl:attribute name="id">shop</xsl:attribute>
	   </xsl:if>
<a href="{@link}" class="act" umi:element-id="{@id}" umi:region="row" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
			<xsl:value-of select="." />
      </a>
<xsl:apply-templates select="document(concat('udata://content/menu/0/1/', @id))/udata[items/item]" mode="sub_menu"/>
       </li>
    </xsl:template>
    <xsl:template match="udata[@method = 'menu']" mode="sub_menu">
		<ul class="sub-menu">
        <xsl:apply-templates select="items/item" mode="sub_menu" />
		</ul>
	</xsl:template>
<xsl:template match="item" mode="sub_menu">
<li>
<a href="{@link}"><xsl:value-of select="." /></a>
</li>
</xsl:template>

    <!-- Catalog menu -->
	<xsl:template match="udata[@method = 'menu']" mode="catalog_menu">
		<xsl:apply-templates select="items/item" mode="catalog_menu" />
	</xsl:template>
	
	<xsl:template match="item" mode="catalog_menu">
		<xsl:variable name="name" select=".//property[@name='main_list_name']/value" />
		<xsl:variable name="img" select=".//property[@name='menu_pic_ua']/value" />
		<a class="catalog_menu" href="{@link}">
			<div class="border_menu_insert">
				<div class="border_menu">
					<!-- <img src="{document(concat('upage://', @id, '.menu_pic_ua'))/udata/property/value}"/> -->
					<img src="{$img}"/>
				</div>
			</div>
			<span>
				<xsl:value-of select="$name" />
				<xsl:if test="not($name)">
					<xsl:value-of select="node()" />
				</xsl:if>
			</span>
		</a>
		<xsl:if test="position() mod 3 = 0">
			<div class="clear"></div>
		</xsl:if>
	</xsl:template>
    
    <!-- catalog_left_menu menu -->
	<xsl:template match="udata[@method = 'menu']" mode="left_menu">
    <div class="border" style="padding-top:20px; padding-bottom:20px; margin-bottom:20px;">
    <ul class="right-nav">
    <xsl:apply-templates select="items/item" mode="left_menu" />
    </ul>
    </div>
	
	</xsl:template>
    
    <xsl:template match="item" mode="left_menu">
    <li><a href="{@link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
   
   
    
    <!-- catalog_left_menu menu -->
	<xsl:template match="udata[@method = 'menu']" mode="catalog_left_menu">
    <div class="border" style="padding-top:20px; padding-bottom:20px; margin-bottom:20px;">
    <ul class="right-nav">
    <xsl:apply-templates select="items/item" mode="catalog_left_menu">
		<xsl:sort select="." />
    </xsl:apply-templates>
    </ul>
    </div>
	
	</xsl:template>
    
    <xsl:template match="item" mode="catalog_left_menu">
    <li class="cat1"><a href="{@link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
   
    <xsl:template match="item[@status = 'active']" mode="catalog_left_menu">
    <style>
	.cat1 { display:none;}
	</style>
    <div style="height:150px; overflow:auto;">
    <li class="cat1"><a href="{@link}"><xsl:value-of select="." /></a></li>
    <xsl:apply-templates select="document(concat('udata://content/menu/0/1/', @id))/udata[items/item]" mode="left_sub_menu"/>
    </div>
    </xsl:template>
    
    <xsl:template match="udata[@method = 'menu']" mode="left_sub_menu">
		<xsl:apply-templates select="items/item" mode="left_sub_menu">
			<xsl:sort select="." />
		</xsl:apply-templates>
    </xsl:template>
    <xsl:template match="item" mode="left_sub_menu">
	<li><a href="{@link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
    
    
    <!-- catalog_left_menu -->
	<xsl:template match="udata[@method = 'menu']" mode="catalog_left_menu_menu">
    <div class="border" style="padding-top:20px; padding-bottom:20px; margin-bottom:20px;">
    <ul class="right-nav">
    <xsl:apply-templates select="items/item" mode="catalog_left_menu_menu">
		<xsl:sort select="." />
    </xsl:apply-templates>
    </ul>
    </div>
	
	</xsl:template>
    
    <xsl:template match="item" mode="catalog_left_menu_menu">
    <li class="cat1"><a href="{@link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
   
    <xsl:template match="item[@status = 'active']" mode="catalog_left_menu_menu">
    <style>
	.cat1 { display:none;}
	</style>
    <div>
    <li class="cat1"><a href="{@link}"><xsl:value-of select="." /></a></li>
    <xsl:apply-templates select="document(concat('udata://content/menu/0/1/', @id))/udata[items/item]" mode="left_sub_menu"/>
    </div>
    </xsl:template>
    
    
	<!-- Footer menu -->
	<xsl:template match="udata[@method = 'menu']" mode="bottom_menu">
	<xsl:apply-templates select="items/item" mode="bottom_menu" />
	</xsl:template>
    <xsl:template match="item" mode="bottom_menu">
	<a href="{@link}"><xsl:value-of select="." /></a>
	</xsl:template>
    
    <!-- Footer menu cat -->
	<xsl:template match="udata[@method = 'menu']" mode="bottom_menu_cat">
	<xsl:apply-templates select="items/item" mode="bottom_menu_cat" />
	</xsl:template>
    <xsl:template match="item" mode="bottom_menu_cat">
	<a style="display:block; padding-bottom:5px; border-bottom:" href="{@link}"><xsl:value-of select="." /></a>
	</xsl:template>
    
</xsl:stylesheet>