<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>
	
	<!-- <xsl:variable name="document-page-id" select="/htmlblock/@pid" /> -->

	<xsl:template match="udata[@method = 'getHtml']"><!--  mode="htmlblock" -->
		<p><xsl:text>test1</xsl:text></p>
	</xsl:template>

	<xsl:template match="htmlblock">
		<p>
			Шаблон <xsl:value-of select="@id" /> не найден
		</p>
		<p>
			<xsl:copy-of select="." />
		</p>
	</xsl:template>
	
	<!-- Список услуг (Лента новостей - Лэндинг) -->

	<xsl:template match="htmlblock[@id='bs3']">
		<div class="services-questions">
			<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
			<xsl:if test="//params/item[@id=2]">
				, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs4' or @id='bl3']">
		<xsl:apply-templates select="document('udata://news/lastlist/1786///1//1/?extProps=publish_time,anons')" mode="services-landing">
			<xsl:with-param name="pid" select="@pid" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs5']">
		<h3 class="sback services-hdr">
			<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
			<xsl:if test="//params/item[@id=2]">
				, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
			</xsl:if>
		</h3>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs6']">
        <section class="sback">
            <div class="container clearfix">
                <div class="services-width33">
					<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
                </div>
                <div class="services-width33">
					<xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
                </div>
                <div class="services-width33">
					<xsl:value-of select="//params/item[@id=3]" disable-output-escaping="yes" />
                </div>
                <!-- <div class="services-width33">
                    <a href="#" class="icon-wrap"><img src="/images/cms/data/services/icon-book.png" alt=""></a>
                    <h4><a href="#">Проектные работы</a></h4>
                    <p><a href="#" class="services-link">Самая важная часть комплекса работ, от качества выполнения которой полностью будет зависеть финальный результат всего проекта. Это стадия, на которой Вы сможете четко определиться с тем, как будет выглядеть объект по окончании работ.</a></p>
                </div> -->
            </div>
        </section>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs6_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="sback"&gt;
	&lt;div class="container clearfix"&gt;
		&lt;div class="services-width33"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs6_middle']">
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
		&lt;div class="services-width33"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs6_end']">
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs7_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="sback"&gt;
	&lt;div class="container sertificate-more clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs7_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs8_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="sback"&gt;
	&lt;div class="container services-order clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs8_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs9']">
		<div class="sback">
			<hr />
		</div>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs10_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="services-fourblock"&gt;
	&lt;div class="container clearfix"&gt;
		&lt;div class="services-width50"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs10_middle']">
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
		&lt;div class="services-width50"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bs10_end']">
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="udata[@method = 'lastlist']" mode="services-landing">
		<xsl:param name="pid" />
        <section class="service landing-menu">
            <div class="container clearfix">
				<xsl:apply-templates select="items/item" mode="services-landing">
					<xsl:with-param name="pid" select="$pid" />
				</xsl:apply-templates>
            </div>
        </section>
	</xsl:template>


	<xsl:template match="item" mode="services-landing">
		<xsl:param name="pid" />
		<!-- <xsl:copy-of select="." /> -->
		<div class="item clearfix active">
			<xsl:attribute name="class">
				item clearfix
				<xsl:if test="@id = $pid"> active</xsl:if>
			</xsl:attribute>
			<a href="{@link}">
				<img src="{document(concat('upage://', @id, '.menu_pic_ua'))//value}" alt="" />
				<div>
					<xsl:value-of select="node()" />
				</div>
			</a>
		</div>
	</xsl:template>
	
	<!-- Templates for TinyMCE -->

	<xsl:template match="htmlblock[@id='tbs3']">
		<hr />
		%content getHtml(bs3,Что мы предлагаем?)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs4']">
		<hr />
		%content getHtml(bs4)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs5']">
		<hr />
		%content getHtml(bs5,Ландшафтное проектирование)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs6']">
		<hr />
		%content getHtml(bs6_start)%
		<hr width="50%" />
			<a href="#" class="icon-wrap"><img src="/images/cms/data/services/icon-photo.png" alt="" /></a>
			<h4><a href="#">Топографическая съемка</a></h4>
			<p>Комплекс работ, выполняемых с целью получения съёмочного оригинала топографических карт или планов местности, а также получение топографической информации в другой форме.</p>
		<hr width="50%" />
		%content getHtml(bs6_middle)%
		<hr width="50%" />
			<a href="#" class="icon-wrap"><img src="/images/cms/data/services/icon-mountin.png" alt="" /></a>
			<h4><a href="#">Предварительный эскиз</a></h4>
			<p>На этом этапе архитектор предлагает различные варианты решения пространства участка, помогает сориентироваться в широком ассортименте материалов, представленных на рынке ландшафтной индустрии, которые подходят для использования именно в Вашем саду.</p>
		<hr width="50%" />
		%content getHtml(bs6_middle)%
		<hr width="50%" />
			<a href="#" class="icon-wrap"><img src="/images/cms/data/services/icon-book.png" alt="" /></a>
			<h4><a href="#">Проектные работы</a></h4>
			<p>Самая важная часть комплекса работ, от качества выполнения которой полностью будет зависеть финальный результат всего проекта. Это стадия, на которой Вы сможете четко определиться с тем, как будет выглядеть объект по окончании работ.</p>
		<hr width="50%" />
		%content getHtml(bs6_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs7']">
		<hr />
		%content getHtml(bs7_start)%
		<hr width="50%" />
			<a href="/images/cms/data/services/icon-sert.png" class="sertificate fancybox"><img src="/images/cms/data/services/icon-sert.png" alt="" /> <span>Сертификат проектных работ</span></a> &#160;&#160;&#160;
			<a href="#" class="more"><span>Читать подробнее о ландшафтном проектировании</span></a>
		<hr width="50%" />
		%content getHtml(bs7_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs8']">
		<hr />
		%content getHtml(bs8_start)%
		<hr width="50%" />
			<span>Общая стоимость проектных работ от 80000 руб.</span>
			<a href="#"><img src="/images/cms/data/services/services-order.png" /></a>
		<hr width="50%" />
		%content getHtml(bs8_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs9']">
		<hr />
		%content getHtml(bs9)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbs10']">
		<hr />
		%content getHtml(bs10_start)%
		<hr width="50%" />
			<h3 class="services-hdr">Озеленение участка</h3>
			<a href="#" class="icon-wrap"><img src="/images/cms/data/services/icon-elka.png" alt="" /></a>
			<ul>
				<li>Высадка деревьев и кустарников</li>
				<li>Создание клумб</li>
				<li>Вертикальное озеленение</li>
				<li>Устройство газонов</li>
				<li>Шумозащита и ветрозащита</li>
				<li>Организация рекреационных зон</li>
			</ul>
			<a href="#" class="more">Читать подробнее об озеленении участка</a>
		<hr width="50%" />
		%content getHtml(bs10_middle)%
		<hr width="50%" />
			<h3 class="services-hdr">Уход за участком</h3>
			<a href="#" class="icon-wrap"><img src="/images/cms/data/services/icon-leika.png" alt="" /></a>
			<ul>
				<li>Уход за газоном</li>
				<li>Уход за водоёмами</li>
				<li>Уход за садом</li>
				<li>Борьба с вредителями</li>
				<li>Внесение удобрений</li>
				<li>Подготовка растений в зиме</li>
			</ul>
			<a href="#" class="more">Читать подробнее об уходе за участком</a>
		<hr width="50%" />
		%content getHtml(bs10_end)%
	</xsl:template>
	
	<!-- Страница услуги (Новость - Лэндинг) -->

	<xsl:template match="htmlblock[@id='bl4_start']">
<xsl:text disable-output-escaping="yes">
&lt;section class="landing-bg" style="background-image:url(</xsl:text>
<xsl:value-of select="document(concat('upage://', @pid, '.publish_pic'))//value" />
<xsl:text disable-output-escaping="yes">);"&gt;
	&lt;div class="container clearfix"&gt;
</xsl:text>
		<xsl:variable name="webform" select="document('udata://webforms/add/364/')/udata" />
		<div class="landing-form">
			<form method="post" action="/webforms/send/">
				<input type="hidden" name="system_email_to" value="{$webform/items/item[@selected='selected']/@id}" />
				<input type="hidden" name="system_form_id" value="{$webform/@form_id}" />
				<input type="hidden" name="system_template" value="webforms" />
				<!-- <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{$webform/@form_id}/" /> -->
				<input type="hidden" class="sitepagelink" name="{$webform/groups/group/field[@name='sitepagelink']/@input_name}" value="" />
				<input type="hidden" class="sitepage" name="{$webform/groups/group/field[@name='sitepage']/@input_name}" value="" />
				<legend>
					<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
					<xsl:if test="//params/item[@id=2]">
						, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
					</xsl:if>
				</legend>
				<xsl:apply-templates select="$webform/groups/group/field[@name!='sitepage' and @name!='sitepagelink']" mode="webforms" />
				<!-- <input type="text" name="" placeholder="Ваше имя" />
				<input type="phone" name="" placeholder="Ваш номер телефона" /> -->
				<input type="submit" value="Заказать звонок" class="call" />
			</form>
		</div>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl4_end']">
		<div class="form-success" style="display:none;">
			<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
			<xsl:if test="//params/item[@id=2]">
				, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
			</xsl:if>
			<xsl:if test="//params/item[@id=3]">
				, <xsl:value-of select="//params/item[@id=3]" disable-output-escaping="yes" />
			</xsl:if>
		</div>
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl5']">
		<h3 class="sback landing-hdr">
			<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
			<xsl:if test="//params/item[@id=2]">
				, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
			</xsl:if>
		</h3>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl6']">
		<div class="landing-design clearfix">
			<h4 class="counter">
				<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
				<xsl:if test="//params/item[@id=2]">
					, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
				</xsl:if>
			</h4>
		</div>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl7_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-design"&gt;
	&lt;div class="container clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl7_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl8_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-design"&gt;
	&lt;div class="container block-2 clearfix"&gt;
		&lt;div class="photos"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl8_middle']">
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl8_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl9_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-design"&gt;
	&lt;div class="container block-3 clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl9_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl10_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-design"&gt;
	&lt;div class="container block-4 clearfix"&gt;
		&lt;div class="photos"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl10_middle']">
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl10_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl11_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-design"&gt;
	&lt;div class="container block-5 clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl11_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl12']">
		<section class="landing-design">
			<div class="container clearfix">
				<div class="video-wrap">
					<iframe width="800" height="450" src="{//params/item[@id=1]}" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
				</div>
			</div>
		</section>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl13_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-works"&gt;
	&lt;div class="container clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl13_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl14_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-social"&gt;
	&lt;div class="container clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl14_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl15_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-questions"&gt;
	&lt;div class="container clearfix"&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl15_end']">
		<xsl:text disable-output-escaping="yes">
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl16_start']">
		<xsl:text disable-output-escaping="yes">
&lt;section class="landing-contacts"&gt;
	&lt;div class="container clearfix"&gt;
		&lt;div class="landing-contacts-form clearfix"&gt;
			&lt;div class="contacts"&gt;
		</xsl:text>
				<div class="form-success" style="display:none;">
					<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
					<xsl:if test="//params/item[@id=2]">
						, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
					</xsl:if>
					<xsl:if test="//params/item[@id=3]">
						, <xsl:value-of select="//params/item[@id=3]" disable-output-escaping="yes" />
					</xsl:if>
				</div>
	</xsl:template>

	<xsl:template match="htmlblock[@id='bl16_end']">
		<xsl:text disable-output-escaping="yes">
			&lt;/div&gt;
		</xsl:text>
		<xsl:variable name="webform" select="document('udata://webforms/add/365/')/udata" />
		<div class="landing-form">
			<p>
				<xsl:value-of select="//params/item[@id=1]" disable-output-escaping="yes" />
				<xsl:if test="//params/item[@id=2]">
					, <xsl:value-of select="//params/item[@id=2]" disable-output-escaping="yes" />
				</xsl:if>
			</p>
			<form method="post" action="/webforms/send/">
				<input type="hidden" name="system_email_to" value="{$webform/items/item[@selected='selected']/@id}" />
				<input type="hidden" name="system_form_id" value="{$webform/@form_id}" />
				<input type="hidden" name="system_template" value="webforms" />
				<input type="hidden" class="sitepagelink" name="{$webform/groups/group/field[@name='sitepagelink']/@input_name}" value="" />
				<input type="hidden" class="sitepage" name="{$webform/groups/group/field[@name='sitepage']/@input_name}" value="" />
				<!-- <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{$webform/@form_id}/" /> -->
				<!-- <input type="text" name="" placeholder="Ваше имя" />
				<input type="phone" name="" placeholder="Телефон" />
				<input type="email" name="" placeholder="E-mail" />
				<textarea rows="7" placeholder="Сообщение"></textarea>  -->
				<xsl:apply-templates select="$webform/groups/group/field[@name!='sitepage' and @name!='sitepagelink']" mode="webforms" />
				<input type="submit" value="Отправить" />
			</form>
		</div>
		<xsl:text disable-output-escaping="yes">
		&lt;/div&gt;
	&lt;/div&gt;
&lt;/section&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template match="field" mode="webforms">
		<input type="text" name="{@input_name}">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">required</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="placeholder"><xsl:value-of select="@title" /></xsl:attribute>
		</input>
	</xsl:template>

	<xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="webforms">
		<textarea name="{@input_name}" rows="7" placeholder="Сообщение">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">required</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="placeholder"><xsl:value-of select="@title" /></xsl:attribute>
		</textarea>
	</xsl:template>
	
	<!-- Templates for TinyMCE -->

	<xsl:template match="htmlblock[@id='tbl3']">
		<hr />
		%content getHtml(bl3)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl4']">
		<hr />
		%content getHtml(bl4_start,Консультация<br /> специалиста:)%
		<hr width="50%" />
            <h1 style="background: rgba(121,180,108,0.8);">Ландшафтное проектирование</h1>
            <h2 style="background: rgba(160,146,134,0.8);">Идеальное отражение Ваших желаний</h2>
		<hr width="50%" />
		%content getHtml(bl4_end,'Звонок на данный номер заказан, Вам перезвонят в течении 10 минут')%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl5']">
		<hr />
		%content getHtml(bl5,Этапы проектирования)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl6']">
		<hr />
		%content getHtml(bl6,Общение с заказчиком)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl7']">
		<hr />
		%content getHtml(bl7_start)%
		<hr width="50%" />
			<p>Проектирование ландшафтного дизайна - это очень важная часть комплекса работ, от качества выполнения которой полностью будет зависеть финальный результат всего проекта. Это стадия, на которой Вы сможете четко определиться с тем, как будет выглядеть объект по окончании работ.</p>
		<hr width="50%" />
		%content getHtml(bl7_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl8']">
		<hr />
		%content getHtml(bl8_start)%
		<hr width="50%" />
			<a href="/images/cms/data/landing/big-photo.jpg" class="landing-gallery" rel="group"><img src="/images/cms/data/landing/big-photo.jpg" class="big-photo" alt="" /></a>
			<a href="/images/cms/data/landing/big-photo.jpg" class="landing-gallery" rel="group"><img src="/images/cms/data/landing/small-photo.jpg" alt="" /></a>
			<a href="/images/cms/data/landing/big-photo.jpg" class="landing-gallery" rel="group"><img src="/images/cms/data/landing/small-photo.jpg" class="center-photo" alt="" /></a>
			<a href="/images/cms/data/landing/big-photo.jpg" class="landing-gallery" rel="group"><img src="/images/cms/data/landing/small-photo.jpg" alt="" /></a>
		<hr width="50%" />
		%content getHtml(bl8_middle)%
		<hr width="50%" />
			<p>Топографическая съёмка — комплекс работ, выполняемых с целью получения съёмочного оригинала топографических карт или планов местности, а также получение топографической информации в другой форме. Выполняется посредством измерений расстояний, высот, углов с помощью различных инструментов.</p>
		<hr width="50%" />
		%content getHtml(bl8_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl9']">
		<hr />
		%content getHtml(bl9_start)%
		<hr width="50%" />
			<img src="/images/cms/data/landing/eskiz.jpg" />
			<a href="/images/cms/data/landing/eskiz.jpg">до</a> &#160;&#160;
			<a href="/images/cms/data/landing/landing-bg-proektirovanie.jpg">процесс</a> &#160;&#160;
			<a href="/images/cms/data/landing/eskiz.jpg">после</a>
		<hr width="50%" />
		%content getHtml(bl9_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl10']">
		<hr />
		%content getHtml(bl10_start)%
		<hr width="50%" />
		   <a href="/images/cms/data/landing/big-photo2.jpg" class="landing-gallery2" rel="group2"><img src="/images/cms/data/landing/big-photo2.jpg" class="big-photo" alt="" /></a>
		   <a href="/images/cms/data/landing/big-photo2.jpg" class="landing-gallery2" rel="group2"><img src="/images/cms/data/landing/small-photo2.jpg" alt="" /></a>
		   <a href="/images/cms/data/landing/big-photo2.jpg" class="landing-gallery2" rel="group2"><img src="/images/cms/data/landing/small-photo2.jpg" class="center-photo" alt="" /></a>
		   <a href="/images/cms/data/landing/big-photo2.jpg" class="landing-gallery2" rel="group2"><img src="/images/cms/data/landing/small-photo2.jpg" alt="" /></a>
		<hr width="50%" />
		%content getHtml(bl10_middle)%
		<hr width="50%" />
			<p>Минимальный состав проекта:</p>
			<ol>
				<li>Генеральный план</li>
				<li>Разбивочный чертёж</li>
				<li>Посадочный план</li>
				<li>Посадочная ведомость</li>
				<li>Схема дренажной системы</li>
				<li>Схема ливневой канализации</li>
				<li>Схема автоматического полива</li>
				<li>Схема расположения светильников</li>
				<li>Разработка схем систем безопасности</li>
				<li>Сметный расчёт</li>
			</ol>
		<hr width="50%" />
		%content getHtml(bl10_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl11']">
		<hr />
		%content getHtml(bl11_start)%
		<hr width="50%" />
			<a href="/images/cms/data/landing/round-img1.png" class="landing-gallery3" rel="group3"><img src="/images/cms/data/landing/round-img1.png" alt="" /></a>
			<a href="/images/cms/data/landing/round-img2.png" class="landing-gallery3" rel="group3"><img src="/images/cms/data/landing/round-img2.png" alt="" /></a>
			<a href="/images/cms/data/landing/round-img3.png" class="landing-gallery3" rel="group3"><img src="/images/cms/data/landing/round-img3.png" alt="" /></a>
		<hr width="50%" />
		%content getHtml(bl11_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl12']">
		<hr />
		%content getHtml(bl12,'https://www.youtube.com/embed/mHj3A1SzIMc')%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl13']">
		<hr />
		%content getHtml(bl13_start)%
		<hr width="50%" />
			<a href="/images/cms/data/landing/works-img1.jpg" class="landing-gallery4" rel="group4"><img src="/images/cms/data/landing/works-img1.jpg" alt="" /></a>
			<a href="/images/cms/data/landing/works-img2.jpg" class="landing-gallery4" rel="group4"><img src="/images/cms/data/landing/works-img2.jpg" alt="" /></a>
			<a href="/images/cms/data/landing/works-img3.jpg" class="landing-gallery4" rel="group4"><img src="/images/cms/data/landing/works-img3.jpg" alt="" /></a>
			<a href="/images/cms/data/landing/works-img4.jpg" class="landing-gallery4" rel="group4"><img src="/images/cms/data/landing/works-img4.jpg" alt="" /></a>
		<hr width="50%" />
		%content getHtml(bl13_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl14']">
		<hr />
		%content getHtml(bl14_start)%
		<hr width="50%" />
			<a href="#"><img src="/images/cms/data/landing/vk-icon.png" alt="" /></a>
			<a href="#"><img src="/images/cms/data/landing/fb-icon.png" alt="" /></a>
			<a href="#"><img src="/images/cms/data/landing/instagram-icon.png" alt="" /></a>
			<a href="#"><img src="/images/cms/data/landing/pen-icon.png" alt="" /></a>
			<a href="#"><img src="/images/cms/data/landing/youtube-icon.png" alt="" class="youtube" /></a>
		<hr width="50%" />
		%content getHtml(bl14_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl15']">
		<hr />
		%content getHtml(bl15_start)%
		<hr width="50%" />
<img src="/images/cms/data/landing/questions-img.jpg" alt="" class="questions-img" />
<ul>
<li>Что такое ландшафтный дизайн и для чего он нужен?</li>
<li>Это не совсем так. Все зависит от возраста растений. Если во время реализации проекта вы используете молодой посадочный материал, то ваш сад действительно будет отличаться от сада на картинках. И это понятно, ведь своего пика декоративности сад достигает к пяти, а то и к десяти годам, когда деревья и кустарники подрастут и заполнят собою пустоты. Если ждать невмоготу, вы всегда сможете использовать на своем участке крупномеры. Но нужно помнить, что такие посадки могут «больно» ударить по вашему кошельку. Подумайте, возможно, увлекательнее будет посадить небольшие растения, наблюдать за их развитием, ухаживать за ними и вместе с ними расти и взрослеть.
</li>
<li>Для чего нужны услуги ландшафтного дизайнера?</li>
<li>Это не совсем так. Все зависит от возраста растений. Если во время реализации проекта вы используете молодой посадочный материал, то ваш сад действительно будет отличаться от сада на картинках. И это понятно, ведь своего пика декоративности сад достигает к пяти, а то и к десяти годам, когда деревья и кустарники подрастут и заполнят собою пустоты. Если ждать невмоготу, вы всегда сможете использовать на своем участке крупномеры. Но нужно помнить, что такие посадки могут «больно» ударить по вашему кошельку. Подумайте, возможно, увлекательнее будет посадить небольшие растения, наблюдать за их развитием, ухаживать за ними и вместе с ними расти и взрослеть.
</li>
<li>Предлагает ли ваша студия услуги непосредственно по озеленению садового участка?</li>
<li>Это не совсем так. Все зависит от возраста растений. Если во время реализации проекта вы используете молодой посадочный материал, то ваш сад действительно будет отличаться от сада на картинках. И это понятно, ведь своего пика декоративности сад достигает к пяти, а то и к десяти годам, когда деревья и кустарники подрастут и заполнят собою пустоты. Если ждать невмоготу, вы всегда сможете использовать на своем участке крупномеры. Но нужно помнить, что такие посадки могут «больно» ударить по вашему кошельку. Подумайте, возможно, увлекательнее будет посадить небольшие растения, наблюдать за их развитием, ухаживать за ними и вместе с ними расти и взрослеть.
</li>
<li>Некоторые организации предлагают сделать проект бесплатно. С чем это связано?</li>
<li>Это не совсем так. Все зависит от возраста растений. Если во время реализации проекта вы используете молодой посадочный материал, то ваш сад действительно будет отличаться от сада на картинках. И это понятно, ведь своего пика декоративности сад достигает к пяти, а то и к десяти годам, когда деревья и кустарники подрастут и заполнят собою пустоты. Если ждать невмоготу, вы всегда сможете использовать на своем участке крупномеры. Но нужно помнить, что такие посадки могут «больно» ударить по вашему кошельку. Подумайте, возможно, увлекательнее будет посадить небольшие растения, наблюдать за их развитием, ухаживать за ними и вместе с ними расти и взрослеть.
</li>
<li>Говорят, что сад, который изображен на картинках дизайнера, не будет соответствовать действительности. Почему?</li>
<li>Это не совсем так. Все зависит от возраста растений. Если во время реализации проекта вы используете молодой посадочный материал, то ваш сад действительно будет отличаться от сада на картинках. И это понятно, ведь своего пика декоративности сад достигает к пяти, а то и к десяти годам, когда деревья и кустарники подрастут и заполнят собою пустоты. Если ждать невмоготу, вы всегда сможете использовать на своем участке крупномеры. Но нужно помнить, что такие посадки могут «больно» ударить по вашему кошельку. Подумайте, возможно, увлекательнее будет посадить небольшие растения, наблюдать за их развитием, ухаживать за ними и вместе с ними расти и взрослеть.
</li>
</ul>
		<hr width="50%" />
		%content getHtml(bl15_end)%
	</xsl:template>

	<xsl:template match="htmlblock[@id='tbl16']">
		<hr />
		%content getHtml(bl16_start,'Ваше сообщение отправлено, спасибо')%
		<hr width="50%" />
			<p>
				Санкт-Петербург,<br /> 
				Выборгское шоссе д. 365<br />
				8 812 702-92-93<br />
				+7 921 952-91-90<br />
				<a href="mailto:principnovo@gmail.com">principnovo@gmail.com</a>
			</p>
		<hr width="50%" />
		%content getHtml(bl16_end,'Мы ответим на Ваши любые вопросы!')%
	</xsl:template>
   
</xsl:stylesheet>