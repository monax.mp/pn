<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink">

		
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']">
		<xsl:apply-templates select="document('udata://emarket/cart')/udata" />
    </xsl:template>

	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step'][udata/onestep]">
    <xsl:apply-templates select="document('udata://emarket/cart')/udata" />
    <style>
	.without-steps .border {padding-top:20px; border:1px solid #CCC; padding-left:20px; padding-bottom:20px; margin-bottom:20px;}
	.without-steps .border h4, h5 {margin-top:0px; margin-bottom:10px; font-size:16px;}
	.without-steps .border #delete h4 {display:none!important;}
	.without-steps .border .textinputs { width:80%; height:25px; display:block; margin-bottom:10px; padding-left:5px;}
	</style>
		<form class="without-steps" action="/emarket/saveInfo" method="POST">
            <div class="border">
            <xsl:apply-templates select="udata/onestep/customer" />
            </div>
            <div class="border"> 
			<xsl:apply-templates select="udata/onestep/delivery_choose" />
            </div>
            <div class="border" id="mydiv" style="display:none;">   
			<xsl:apply-templates select="udata/onestep/delivery" />
            </div>
            <div class="border">
			<xsl:apply-templates select="udata/onestep/payment" />
            </div>
			<input type="submit" value="Оформить заказ" class="button big green-button" />
		</form>
	</xsl:template>

	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/customer">
		<div class="customer">
			<xsl:apply-templates select="document(concat('udata://data/getEditForm/', @id))" />
		</div>
	</xsl:template>
	
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery">
		<div class="delivery" id="delete">
        <h5>Адрес доставки:</h5>
			<xsl:apply-templates select="document(@xlink:href)" />
		</div>
	</xsl:template>
	
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery[items/item]">
		<div class="delivery">
			<h4>Адрес доставки:</h4>
			<xsl:apply-templates select="items" mode="delivery-address" />
		</div>
	</xsl:template>
	
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery_choose" />
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery_choose[items/item]">
		<div class="dychoose">
			<h4>Способ доставки:</h4>
			<!--<xsl:apply-templates select="items" mode="delivery-choose" />-->

<script type="text/javascript">
function Show(a) {
        obj=document.getElementById("mydiv");
        if (a) obj.style.display="block";
        else obj.style.display="none";
}
</script> 
<div>
<label>
<input type="radio" checked=""  onClick="Show(0);" value="584" name="delivery-id"/>
Самовывоз по адресу: г. Санкт-Петербург, Выборгское шоссе д.365
</label>
</div>
<!-- <div>
<label>
<input type="radio" value="585"  onClick="Show(1);" name="delivery-id"/>
Курьерская доставка: стоимость доставки уточняйте у менеджера
</label>
</div> -->
</div>			
</xsl:template>
	
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/payment" />
	<xsl:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/payment[items/item]">
		<div class="payment">
			<h4>Способ оплаты:</h4>
			<xsl:apply-templates select="items/item" mode="payment_one_step" />
		</div>
	</xsl:template>
</xsl:stylesheet>