<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="purchase/required.xsl" />
	<xsl:include href="purchase/delivery.xsl" />
	<xsl:include href="purchase/payment.xsl" />

	<xsl:template match="/result[@method = 'purchase']">
		<xsl:apply-templates select="document('udata://emarket/purchase')" />
	</xsl:template>
	
	<xsl:template match="purchasing">
		<h4>
			<xsl:text>Purchase is in progress: </xsl:text>
			<xsl:value-of select="concat(@stage, '::', @step, '()')" />
		</h4>
	</xsl:template>
	
	<xsl:template match="purchasing[@stage = 'result']">
		<p>
			<xsl:text>&emarket-order-failed;</xsl:text>
		</p>
	</xsl:template>
	
	<xsl:template match="purchasing[@stage = 'result' and @step = 'successful']">
		<!--<xsl:apply-templates select="//steps" />-->
<div class="otziv">
<div class="title"><strong>Ваш <xsl:value-of select="document(concat('uobject://', //order/@id))/udata/object/@name" /> поступил в обработку!</strong></div>	
<div class="message-layrs">
<div class="message">
Наш менеджер свяжется с Вами в ближайшее время для уточнения информации по Вашему заказу.<br/><br/>

Если Вы зарегистрированный пользователь, то историю и статус Вашего заказа Вы можете просматривать в личном кабинете во вкладке заказы.<br/><br/>

Спасибо за заказ! 
</div>
</div>
</div>
<div class="cart-buttons">
<a href="/shop/" class="toCatalog green-button">&continue-shopping;</a>
</div>
	</xsl:template>
</xsl:stylesheet>