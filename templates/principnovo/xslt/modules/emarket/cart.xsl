<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="result[@method = 'cart']">
		<xsl:apply-templates select="document('udata://emarket/cart')/udata" />
        
    </xsl:template>

	<xsl:template match="udata[@method = 'cart']">
		<div class="basket">
			<h4 class="empty-content">&basket-empty;</h4>
			
		</div>
	</xsl:template>


	<xsl:template match="udata[@method = 'cart'][count(items/item) &gt; 0]">
		<div class="basket">
			<table width="100%" cellpadding="5" border="0" cellspacing="0">
				<thead>
					<tr style=" background:#82b064; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2) inset; color:#fff; text-align:center; font-size:14px;">
						<td style="border:1px solid #CCC;">
							<xsl:text>&basket-item;</xsl:text>
						</td>
						<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC;">
							<xsl:text>&price;, </xsl:text><xsl:value-of select="$currency-prefix" /><xsl:value-of select="$currency-suffix" /> x <xsl:text>&amount;</xsl:text>
						</td>
						<td style="border:1px solid #CCC;">
							<xsl:text>&item-discount;, </xsl:text><xsl:value-of select="$currency-prefix" /><xsl:value-of select="$currency-suffix" />
						</td>
						<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC;">
							<xsl:text>&sum;, </xsl:text><xsl:value-of select="$currency-prefix" /><xsl:value-of select="$currency-suffix" />
						</td>
						<td style="border-top:1px solid #CCC; color:#82b064; border-bottom:1px solid #CCC;">
							<xsl:text>.</xsl:text>
						</td>
					</tr>
				</thead>
				<tbody>
					<xsl:apply-templates select="items/item" />
				</tbody>
			</table>
			<div class="summary">
				<xsl:apply-templates select="summary" />
			</div>
			<div class="cart-buttons" style="margin-top:20px; margin-bottom:40px;">
	<table width="100%" cellpadding="5" border="0" cellspacing="0">
    <!-- <tr>
      <td style="text-align:left;"><a href="/shop/" class="groow-button">В каталог</a></td>
      <td style="text-align:right;">
		<a href="/emarket/purchasing_one_step/" class="green-button">Оформить заказ</a>	
        </td>
        </tr>-->
        </table>		
				
			</div>
			<div class="clear"></div>
        </div>
    </xsl:template>

	<xsl:template match="udata[@method = 'cart']//item">
		<tr class="cart_item_{@id}">
			<td class="name"  style="border-left:1px solid #CCC; border-bottom:1px solid #CCC; text-align:center;">
				<xsl:call-template name="catalog-thumbnail">
					<xsl:with-param name="element-id" select="page/@id" />
					<xsl:with-param name="field-name">photo</xsl:with-param>
					<xsl:with-param name="empty">&empty-photo;</xsl:with-param>
					<xsl:with-param name="width">77</xsl:with-param>
					<xsl:with-param name="height">55</xsl:with-param>
					<xsl:with-param name="align">middle</xsl:with-param>
				</xsl:call-template>
				<a href="{$lang-prefix}{page/@link}" style="color:#333; text-decoration:none; display:block; text-align:center; margin-top:5px;"><xsl:value-of select="@name" /></a>
			</td>
			<td style="border-left:1px solid #CCC; border-bottom:1px solid #CCC; text-align:center;">
				<span><xsl:value-of select="price/actual | price/original" /></span>
				<span class="x"> x </span>
				<input type="text" style="width:40px; margin-right:5px; text-align:center;" value="{amount}" class="amount" />
				<input type="hidden" value="{amount}" />
				<span class="change-amount">
                <span class="bottom" style="background: #ccc;
    height: 21px;
    width: 21px;
    display: inline-block;
    vertical-align: bottom;
    color: #fff;
    font-weight: bold;
    cursor: pointer;">-</span>
                <span class="top" style="background: #82B064;
    height: 21px;
    width: 21px;
    display: inline-block;
    vertical-align: bottom;
    color: #fff;
    font-weight: bold;
    cursor: pointer;">+</span>
					<!--<img class="top" src="/templates/principnovo/images/amount-top.png"/>
                    
                    
					<img class="bottom" src="/templates/principnovo/images/amount-bottom.png"/>-->
				</span>
			</td>
			<td style="border-left:1px solid #CCC; border-bottom:1px solid #CCC; text-align:center;">
				<span class="cart_item_discount_{@id}">
					<xsl:choose>
						<xsl:when test="discount">
							<xsl:value-of select="discount/amount" />
						</xsl:when>
						<xsl:otherwise>
							0
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</td>
			<td style="border-left:1px solid #CCC; border-bottom:1px solid #CCC; text-align:center;">
				<span class="cart_item_price_{@id} size2">
					<xsl:value-of select="total-price/actual" />
				</span>
			</td>
			<td style="border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; text-align:center;">
				<a href="{$lang-prefix}/emarket/basket/remove/item/{@id}/" style="color:#333; text-decoration:none;" id="del_basket_{@id}" class="del">x</a>
			</td>
		</tr>
	</xsl:template>
	
	
	
	

	<xsl:template match="udata[@method = 'cart']/summary">
    <table width="100%" cellpadding="5" border="0" cellspacing="0">
     <tr>
      <td style="color:#FFF;">.</td>
      <td style="text-align:right; font-size:18px;">
		<xsl:if test="price/bonus!=''">
			<div class="info">
				<xsl:text>&order-bonus;: </xsl:text>
				<span class="cart_discount">
					<xsl:value-of select="$currency-prefix" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="price/bonus" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="$currency-suffix" />
				</span>
			</div>
		</xsl:if>
		<div class="info">
			<xsl:text>&order-discount;: </xsl:text>
			<span class="cart_discount">
				<xsl:value-of select="$currency-prefix" />
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="price/discount!=''">
						<xsl:value-of select="price/discount" />
					</xsl:when>
					<xsl:otherwise>
						0
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$currency-suffix" />
			</span>
		</div>
		<xsl:apply-templates select="price/delivery[.!='']" mode="cart" />
		<div class="size2 tfoot">
			<xsl:text>&summary-price;: </xsl:text>
			<xsl:value-of select="$currency-prefix" />
			<xsl:text> </xsl:text>
			<span class="cart_summary size3">
				<xsl:apply-templates select="price/actual" />
			</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="$currency-suffix" />
		</div>
        </td>
    </tr>
    </table>
	</xsl:template>

	<xsl:template match="delivery[.!='']" mode="cart">
		<div class="info">
			<xsl:text>&delivery;: </xsl:text>
			<xsl:value-of select="$currency-prefix" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="." />
			<xsl:text> </xsl:text>
			<xsl:value-of select="$currency-suffix" />
		</div>
	</xsl:template>
    
   
</xsl:stylesheet>