<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="udata" mode="right-column-news" />

	<xsl:template match="udata[items]" mode="right-column-news">
		<xsl:variable name="category-name" select="document(concat('upage://', category_id))/udata/page/name" />

		<xsl:apply-templates select="items/item" mode="right-column-news" />
					
	</xsl:template>

	<xsl:template match="item" mode="right-column-news">
		<xsl:variable name="item-info" select="document(@xlink:href)" />
<div class="info">
  <div style="text-align:right; padding-bottom:5px; padding-right:5px; font-style:italic;"><xsl:apply-templates select="$item-info//property[@name = 'publish_time']" /></div>
  <div><a class="news-titles" href="/o-nas/sobytiya/postuplenie-rastenij/"><xsl:value-of select="." /></a></div>
</div>
</xsl:template>
</xsl:stylesheet>