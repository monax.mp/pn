<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- <xsl:template match="/result[@module = 'news'][@method = 'rubric'][page/@type-id = 363]"> -->
	<xsl:template match="/result[@module = 'news'][@method = 'rubric'][page/@id = 1786]">
		<!-- <xsl:apply-templates select="document('udata://news/lastlist/?extProps=publish_time,anons')" mode="services-landing" /> -->
		<xsl:value-of select=".//property[@name = 'contenthtml']/value" disable-output-escaping="yes" />
	</xsl:template>
	
	<xsl:template match="/result[@module = 'news'][@method = 'item'][//page/@id = 1786]">
		<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
	</xsl:template>
	
</xsl:stylesheet>