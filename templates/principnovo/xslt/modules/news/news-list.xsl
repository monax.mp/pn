<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="/result[@module = 'news'][@method = 'rubric']">
		<xsl:apply-templates select="document('udata://news/lastlist/?extProps=publish_time,anons')" />
	</xsl:template>

	<xsl:template match="udata[@method = 'lastlist']">
		<div class="news" umi:button-position="top right" umi:element-id="{$document-page-id}"
			umi:region="list" umi:module="news" umi:method="lastlist" umi:sortable="sortable">
			<xsl:apply-templates select="items/item" mode="news-list" />
        <div class="clear"></div>
		</div>
		<xsl:apply-templates select="total" />
	</xsl:template>


	<xsl:template match="item" mode="news-list">
    <div class="box50re">
     <xsl:if test="position() mod 2 = 0">
		<xsl:attribute name="class">box50le</xsl:attribute>	
	 </xsl:if>
      <h2 style="color:#82b064; border-bottom:1px solid #82b064;"><xsl:value-of select="node()" /><xsl:text>&#160;&#160;&#160;</xsl:text></h2>
      <a  href="{@link}"><img src="{document(concat('upage://', @id, '.anons_pic'))//value}" /></a>
      <div style="height:150px; overflow:hidden;"><xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" /></div>
      <div style="padding-top:20px;">
        <div class="box50">
         <a class="green-button" href="{@link}">Продолжить чтение</a>
        </div>
        <div class="clear"></div>
      </div>
     </div>
	</xsl:template>
</xsl:stylesheet>