<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:xlink="http://www.w3.org/TR/xlink">

<xsl:template match="/result[@module = 'news'][@method = 'item']">
<div style="margin-bottom:20px;"><img src="{.//property[@name = 'anons_pic']/value}" style="width:100%;" /></div>
<div umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
</div>
<a class="green-button" href="../">
<xsl:text>Назад к услугам</xsl:text>
</a>
</xsl:template>
</xsl:stylesheet>