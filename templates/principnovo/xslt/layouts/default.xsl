<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:param name="tmode" select="0" />
	<xsl:template match="/" mode="layout">
		<html>
			<head>
            <meta name="viewport" content="width=device-width"/>
                <meta name='yandex-verification' content='7860724901138559' />
				<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> -->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<!-- <meta name="keywords" content="{//meta/keywords}" />
				<meta name="description" content="{//meta/description}" />
				<title><xsl:value-of select="$document-title" /></title> -->
				<xsl:choose>
					<xsl:when test="$p &gt; 0 and $method = 'category'">
						<title><xsl:value-of select="$document-title" />, страница товаров <xsl:value-of select="$p+1" /></title>
						<meta name="keywords" content="{//meta/keywords}, страница товаров {$p+1}" />
						<meta name="description" content="" />
					</xsl:when>
					<xsl:when test="$p &gt; 0">
						<title><xsl:value-of select="$document-title" />, страница товаров <xsl:value-of select="$p+1" /></title>
						<meta name="keywords" content="{//meta/keywords}, страница товаров {$p+1}" />
						<meta name="description" content="" />
					</xsl:when>
					<xsl:otherwise>
						<title><xsl:value-of select="$document-title" /></title>
						<meta name="keywords" content="{//meta/keywords}" />
						<meta name="description" content="{//meta/description}" />
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="document('udata://system/includeQuickEditJs')/udata" disable-output-escaping="yes" />
        <script type="text/javascript" charset="utf-8" src="/js/jquery/fancybox/helpers/jquery.fancybox-thumbs.js"></script>
        <link type="text/css" rel="stylesheet" href="/js/jquery/fancybox/helpers/jquery.fancybox-thumbs.css" />         

				<script type="text/javascript" src="/templates/principnovo/js/i18n.{result/@lang}.js"></script>
				<script type="text/javascript" charset="utf-8" src="/templates/principnovo/js/__common.js"></script>
                <script type="text/javascript" charset="utf-8" src="/templates/principnovo/js/forms-bask.js"></script>
<link href="/templates/principnovo/css/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<link type="text/css" rel="stylesheet" href="/templates/principnovo/css/common.css" />         
<link media="print" type="text/css" rel="stylesheet" href="/templates/principnovo/css/print.css" />
<script>
$(document).ready(function() {
    $("a.scrollto").click(function () {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });
});
</script>
<script  type="text/javascript">
$(function() {
      
      $("<select></select>").appendTo("#mobile-menu");
      $("<option></option>", {
         "value"   : "",
		 "text"    : "Навигация..."
      }).appendTo("nav select");

      $("nav ul li a").each(function() {
       var el = $(this);
       $("<option></option>", {
           "value"   : el.attr("href"),
           "text"    : el.text()
       }).appendTo("nav select");
      });
	  // чтобы выпадающий список дейстивтельно работал
     $("nav select").change(function() {
        window.location = $(this).find("option:selected").val();
      });

});

</script>


<script type="text/javascript">
$(document).ready(function() {
    $("a.fen").fancybox();
});
</script>
<script type='text/javascript'> 
    $(document).ready(function() { 
      $("A#trigger").toggle(function() { 
        // Отображаем скрытый блок 
        $("DIV#box").fadeIn(); // fadeIn - плавное появление
        return false; // не производить переход по ссылке
      },  
      function() { 
        // Скрываем блок 
        $("DIV#box").fadeOut(); // fadeOut - плавное исчезновение 
        return false; // не производить переход по ссылке
      }); // end of toggle() 
    }); // end of ready() 
  </script> 
<script type="text/javascript">
	
jQuery(function($){
	
	if ($('ul#topkar').length) {
		$('ul#topkar').easyPaginate({
			step:1
		});	
	}

});    
    
</script>
<link type="text/css" rel="stylesheet" href="/templates/principnovo/css/flexslider.css"/>
<script type="text/javascript" charset="utf-8" src="/templates/principnovo/js/jquery.flexslider.js"></script>
<script>

$(window).load(function() {
  $('#carouselka').flexslider({
    animation: "fade",
	slideshowSpeed: 4000,
    animationSpeed: 600,
	minItems: 1,
    maxItems: 1
  });
  
   $('#offers').flexslider({
    animation: "slide",
    animationLoop: false,
	itemWidth: 200,
    itemMargin: 5,
	controlNav: false, 
    minItems: 1,
    maxItems: 3
	
  });
  

$('#topslider').flexslider({
    animation: "fade",
	animationDuration: 500,
	slideshowSpeed: 8000,
	animationSpeed: 900
	
  });


$('.flexslider').flexslider({
    animation: "slide",
	slideshow: false
  });
});
</script>

<style>
#carousel .flex-direction-nav
{
	display:none;
}
#carousel .flex-control-nav li
{ width:20px; height:20px; background:none; border-radius:10px; border:1px solid #FFF;}
#carousel .flex-control-nav a
{ width:14px; height:14px; background:none; margin:3px;}
#carousel .flex-control-nav .flex-active
{ width:14px; height:14px; background:#fff;}
#carouselka .flex-direction-nav
{
	display:none;
}
#carouselka li a { text-decoration:none; font-size:22px; margin-left:22px; padding-top:12px; padding-bottom:12px;color: #fff;}
#carouselka ol { display:none;}

#topslider .flex-direction-nav .flex-prev { left:90px; width:70px; height:70px; background:url(/templates/principnovo/images/buttons.png) no-repeat  0px -193px;}
#topslider .flex-direction-nav .flex-next { right:90px; width:70px; height:70px; background:url(/templates/principnovo/images/buttons.png) no-repeat 0px 5px;}
</style>

<!-- <xsl:if test="result/page/@type-id = 363"> -->
<xsl:if test="result//page/@id = 1786">
	<link type="text/css" rel="stylesheet" href="/templates/principnovo/css/landing.css?v=1"/>
	<script type="text/javascript" charset="utf-8" src="/templates/principnovo/js/landing.js"></script>
</xsl:if>

			</head>
			<body>
				
                
<header id="top">
<xsl:choose>
<xsl:when test="/result[@method = 'category']">
<section id="mine" style="
    border-bottom:1px solid #CCC; 
    padding:5px 0px; 
    background:#f2f2f2;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1) inset;
    color: #333;
    text-decoration: none;
    ">
 <div class="container">
   <div class="left-col">
      Тел:  <strong>8 (921) 952-91-90</strong>
    </div>
  
    <div class="right-col" style="text-align:right;">
    <a href="#kab" style="color:#333;" class="fen">Вход</a> | <a style="color:#333;" href="/users/registrate/" class="fen">Регистрация</a>
    </div>
    <div class="clear"></div>
 </div>
</section>
</xsl:when>
<xsl:when test="/result[@method = 'object']">
<section id="mine" style="
    border-bottom:1px solid #CCC; 
    padding:5px 0px; 
    background:#f2f2f2;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1) inset;
    color: #333;
    text-decoration: none;
    ">
 <div class="container">
   <div class="left-col">
      Тел:  <strong>8 (921) 952-91-90</strong>
    </div>
  
    <div class="right-col" style="text-align:right;">
    <a href="#kab" style="color:#333;" class="fen">Вход</a> | <a href="/users/registrate/" style="color:#333;" class="fen">Регистрация</a>
    </div>
    <div class="clear"></div>
 </div>
</section>
</xsl:when>
<xsl:when test="result/@request-uri = '/emarket/purchasing_one_step/'">
<section id="mine" style="
    border-bottom:1px solid #CCC; 
    padding:5px 0px; 
    background:#f2f2f2;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1) inset;
    color: #333;
    text-decoration: none;
    ">
 <div class="container">
   <div class="left-col">
      Тел:  <strong>8 (921) 952-91-90</strong>
    </div>
  
    <div class="right-col" style="text-align:right;">
    <a href="#kab" style="color:#333;" class="fen">Вход</a> | <a href="/users/registrate/" style="color:#333;" class="fen">Регистрация</a>
    </div>
    <div class="clear"></div>
 </div>
</section>
</xsl:when>
<xsl:otherwise>
<section id="mine" style="
    border-bottom:1px solid #CCC; 
    padding:5px 0px; 
    background:#f2f2f2;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1) inset;
    color: #333;
    text-decoration: none;
    ">
 <div class="container">
   <div class="left-col">
      Тел:  <strong>8 (812) 702-92-93</strong>
    </div>
    <div class="right-col" style="text-align:right;">
    <a href="https://vk.com/club76869534" rel="publisher"><img class="social" style="width:20px; height:auto;" src="/templates/principnovo/img/vk.jpg"/></a>
    <a href="https://www.facebook.com/groups/874630445880982/" rel="publisher"><img class="social" style="width:20px; height:auto;" src="/templates/principnovo/img/ff.jpg"/></a>
    <a href="https://plus.google.com/101384926189699797997/" rel="publisher"><img class="social" style="width:20px; height:auto;" src="/templates/principnovo/img/gp.jpg"/></a>
    </div>
    <div class="clear"></div>
 </div>
</section>
</xsl:otherwise>
</xsl:choose>
<xsl:choose>
	<xsl:when test="$tmode != '1'">
		<nav>
			<div id="menu" style="position:relative;">
				<div class="logo"><a href="/"><img src="/templates/principnovo/img/logo_green.png" style="width:100%; height:auto;" /></a></div>
				<div id="mobile-menu"></div>
				<xsl:apply-templates select="document('udata://menu/draw/top_menu')/udata" mode="top_menu_draw" />
				<div class="sear" style="position:relative;"><a style="cursor:pointer;" id="trigger" class="trusts"><img src="/templates/principnovo/img/searchbox_magnifier.png"/></a>
					<div id="box" style="padding-top: 16px;
					padding-bottom: 17px;
					position: absolute;
					z-index: 200;
					display:none;
					background: rgb(255, 255, 255);
					width: 300px;
					right: 69px;
					top: 0px;">
						<xsl:call-template name="search-form-left-column" />
					</div>
				</div>
				<div class="clear"></div>
		   </div>
		</nav>
	</xsl:when>
	<xsl:otherwise>
		<nav>
			<div id="menu" style="position:relative;">
				<div class="logo"><a href="/"><img src="/templates/principnovo/img/logo_green.png" style="width:100%; height:auto;" /></a></div>
				<div id="mobile-menu"></div>
				<xsl:apply-templates select="document('udata://content/menu/(null)/1/')" mode="top_menu" />
				<div class="sear" style="position:relative;"><a style="cursor:pointer;" id="trigger" class="trusts"><img src="/templates/principnovo/img/searchbox_magnifier.png"/></a>
					<div id="box" style="padding-top: 16px;
					padding-bottom: 17px;
					position: absolute;
					z-index: 200;
					display:none;
					background: rgb(255, 255, 255);
					width: 300px;
					right: 69px;
					top: 0px;">
						<xsl:call-template name="search-form-left-column" />
					</div>
				</div>
				<div class="clear"></div>
		   </div>
		</nav>
	</xsl:otherwise>
</xsl:choose>
</header>

<!-- слайдер -->
<xsl:choose>
<xsl:when test="result[page/@is-default = '1']">

<xsl:variable name="main_content" select="document(concat('udata://content/menu///',result/@pageId))" />
<section class="slider">
<!-- <xsl:copy-of select="$main_content"/> -->
<div id="topslider" class="flexslider" style="margin:0px;"> 
<ul class="slides">

	<!-- <xsl:copy-of select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_slider']/@id,'?extProps=menu_pic_ua,content'))"/> -->
	<!-- <xsl:apply-templates select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_uslugi']/@id,'?extProps=h1,menu_pic_ua,u_page,short_description'))/udata//item" mode="main-uslugi" /> -->
	<xsl:apply-templates select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_slider']/@id,'?extProps=menu_pic_ua,content'))/udata//item" mode="main-slider" />
</ul>
</div>




</section>
<!-- слайдер -->
<!-- дискрипт -->
<section class="descript" id="des">
 <div id="carouselka" class="flexslider" style="margin:0px;">
  <ul class="slides">
  <li><a href="#" style="background:url(/templates/principnovo/img/phone.png) left center no-repeat; padding-left:35px;">Звоните для мгновенной консультации +7 (921) 952 91 90</a></li>
  <li><a href="/shop/">Большое поступление растений. Спешите за покупками в наш садовый центр!</a></li>  
</ul>
 </div>
</section>
</xsl:when>
<xsl:when test="$tmode = '2'">
<section class="slider">

<div id="topslider" class="flexslider" style="margin:0px;"> 
<ul class="slides">
<li>
<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 32px; margin-bottom:10px; display:inline-block;">
Ландшафтное строительство
</div>
<br/>
<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 24px; display:inline-block;">
Преобразуйте пространство вокруг себя
</div>
</div>
<img src="/templates/principnovo/img/bg/3s.jpg" alt="" height="450"/>
</li>
<li>
<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 32px; margin-bottom:10px; display:inline-block;">
Продажа растений
</div>
<br/>
<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 24px; display:inline-block;">
Лучший выбор и приятная цена
</div>
</div>
<img src="/templates/principnovo/img/bg/4s.jpg" alt=""  height="450"/></li> 
<li>
<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 32px; margin-bottom:10px; display:inline-block;">
Уход за участком
</div>
<br/>
<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 24px; display:inline-block;">
Сохраним здоровье вашего сада
</div>
</div>
<img src="/templates/principnovo/img/bg/2s.jpg" alt=""  height="450"/></li> 



<li>
<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 32px; margin-bottom:10px; display:inline-block;">
Озеленение
</div>
<br/>
<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 24px; display:inline-block;">
Мы знаем, как создать гармонию в Вашем саду!
</div>
</div>
<img src="/templates/principnovo/img/bg/3s.jpg" alt="" height="450"/>
</li>
<li>
<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 32px; margin-bottom:10px; display:inline-block;">
Строительство
</div>
<br/>
<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 24px; display:inline-block;">
Качество воплощенное в жизнь!
</div>
</div>
<img src="/templates/principnovo/img/bg/4s.jpg" alt=""  height="450"/></li> 
<li>
<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 32px; margin-bottom:10px; display:inline-block;">
Ландшафтное проектирование
</div>
<br/>
<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
color:#FFF!important;
font-weight: 400;
padding:8px 20px;
font-size: 24px; display:inline-block;">
Идеальное отражение Ваших желаний!
</div>
</div>
<img src="/templates/principnovo/img/bg/2s.jpg" alt=""  height="450"/></li> 
</ul>
</div>




</section>
<!-- слайдер -->
<!-- дискрипт -->
<section class="descript" id="des">
 <div id="carouselka" class="flexslider" style="margin:0px;">
  <ul class="slides">
  <li><a href="#" style="background:url(/templates/principnovo/img/phone.png) left center no-repeat; padding-left:35px;">Звоните для мгновенной консультации +7 (921) 952 91 90</a></li>
  <li><a href="/shop/">Большое поступление растений. Спешите за покупками в наш садовый центр!</a></li>  
</ul>
 </div>
</section>
</xsl:when>
<xsl:when test="result//page/@id = 1786">
</xsl:when>
<xsl:otherwise>
<section class="bred">
<xsl:apply-templates select="document('udata://core/navibar')/udata"/>
</section>
</xsl:otherwise>
</xsl:choose>
<!-- дискрипт -->
<!-- главная -->
<xsl:choose>
<xsl:when test="result[page/@is-default = '1']">
<xsl:variable name="main_content" select="document(concat('udata://content/menu///',result/@pageId))" />
<!-- <xsl:copy-of select="$main_content//item[@alt-name='m_uslugi']"/> -->

<section class="service">
 <div class="container">
	<xsl:apply-templates select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_uslugi']/@id,'?extProps=h1,menu_pic_ua,u_page,short_description'))/udata//item" mode="main-uslugi" />   
<div class="clear"></div>
 </div>
</section>

<section class="company">
 <div class="bloks">
   <div class="container">
     <div class="left-grid">
       <h1> <xsl:value-of select=".//property[@name = 'm_texth']/value" disable-output-escaping="yes" /></h1>
       <xsl:value-of select=".//property[@name = 'm_textc']/value" disable-output-escaping="yes" />
       
     </div>
     <div class="right-grid">
		<xsl:apply-templates select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_gal']/@id,'?extProps=menu_pic_ua'))/udata//item" mode="main-gal" />      
     <div class="clear"></div>
     <p style="padding-top:20px;">
       <a class="grey-button" href="/portfolio/">Смотреть портфолио</a>
       </p>
     </div>
<div class="clear"></div>
   </div>
 </div>
</section>

<section class="teem">
<style>
.item-teem { position:relative; text-decoration:none; z-index:10;}
.teem-inf { width:100%; height:100%; position:absolute; bottom:0px; text-align:center; transition:0.7s; background:rgba(0,0,0,0.5); font-size:14px; opacity:0;}
.teem-inf span {font-size:14px; text-transform:uppercase; padding-top:40%;  display:block;}
.item-teem:hover .teem-inf { height:100%; transition:0.7s; opacity:1;}
.tehnology-item a { position:relative; text-decoration:none;  display:block; overflow:hidden;}
.tehnology-item .inf { width:80%; height:0px; position:absolute; bottom:0px; text-align:center; transition:0.7s; font-size:14px; overflow:hidden;}
.tehnology-item a:hover .inf { height:70%; transition:0.7s;}
.tehnology-item:hover img { opacity:0.3!important;}
.news-titles {color:#FFF; text-decoration:none;}
.news-titles:hover { color:#999;}
</style>
 <div class="container">
  <h3>Наша команда</h3>
  <h4>Профессионалы из России, Германии, Франции и Швеции<br />готовы выполнить ваш проект</h4>
<div class="slide">
	<!-- <xsl:copy-of select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_team']/@id,'?extProps=menu_pic_ua,content'))/udata//item" /> -->
	<div id="carousel" class="flexslider">
		<ul class="slides">
		<xsl:apply-templates select="document(concat('udata://content/menu///',$main_content//item[@alt-name='m_team']/@id,'?extProps=menu_pic_ua,content'))/udata" mode="main-team" />
	</ul>
		<!-- <xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" /> -->
	</div>   
<div class="clear"></div>
</div>   
 </div>
</section>


<section class="newsblock">
<div class="container">
 <div class="coluns" style="background:rgba(0, 0, 0, 0.28);">
  <span>Технологии</span>
  <div class="tehnology">
   <div class="tehnology-item">
<a href="/uslugi/landshaftnoe-proektirovanie/"><img src="/templates/principnovo/img/home_features_ideas.png"/>
<div class="inf">
Ландшафтное<br />проектирование
</div>
</a>
   </div>
   <div class="tehnology-item">
<a href="/uslugi/landshaftnoe-stroitelstvo/"><img src="/templates/principnovo/img/home_features_strategy.png"/>
<div class="inf">
Ландшафтное<br />строительство
</div>
</a>
   </div>
   <div class="tehnology-item">
<a href="/uslugi/ozelenenie/"><img src="/templates/principnovo/img/home_features_processes.png"/>
<div class="inf">
Озеленение
</div>
</a>
   </div>
   <div class="tehnology-item">
<a href="/shop/"><img src="/templates/principnovo/img/home_features_contact.png"/>
<div class="inf">
Продажа<br />растений
</div>
</a>
   </div>
   <div class="clear"></div>
  </div>
 <!-- <a class="grey-button" style="margin-left:20px;" href="/uslugi/">Все статьи</a> -->
 </div>
 <div class="coluns"  style="background:rgba(0, 0, 0, 0.15);">
  <span>Контакты</span> 
	<xsl:value-of select=".//property[@name = 'm_contacts']/value" disable-output-escaping="yes" />
  
 </div>
 <div class="coluns"  style="background:rgba(0, 0, 0, 0.15);">
  <span>Последние события</span>
  <xsl:apply-templates select="document('udata://news/lastlist/(/o-nas/sobytiya/)/notemplate/2/0')" mode="right-column-news" />
 </div>
 <div class="coluns"  style="background:rgba(0, 0, 0, 0.1);">
  <span>Отзывы</span>
  <xsl:value-of select=".//property[@name = 'm_feed']/value" disable-output-escaping="yes" />
 
  
 </div>
 <div class="clear"></div>
</div> 
</section>
<section class="maps">
<xsl:value-of select=".//property[@name = 'yandeks']/value" disable-output-escaping="yes" />
</section>
<!-- главная -->
</xsl:when>
<xsl:when test="result[parents/page/@is-default = '1']">

	<xsl:value-of select="document(concat('udata://content/redirect/', '/'))" />
</xsl:when>
<xsl:when test="$tmode = '2'">
<section class="service">
 <div class="container">
   <div class="item">
    <a href="/uslugi/landshaftnoe-proektirovanie/">
    <img src="/templates/principnovo/img/home_features_ideas.png"/>
    <div style="margin-left:120px; padding-right:20px;">
     Ландшафтное проектирование
     <span>Качество, европейских подход, уникальность проектов.</span>
    </div>
    <div class="clear"></div>
    </a>
   </div>
   <div class="item">
    <a href="/uslugi/landshaftnoe-stroitelstvo/">
    <img src="/templates/principnovo/img/home_features_strategy.png"/>
    <div  style="margin-left:120px; padding-right:20px;">
     Ландшафтное строительство
     <span>Опыт, качество, долгосрочный результат.</span>
    </div>
    <div class="clear"></div>
    </a>
   </div>
   <div class="item">
    <a href="/uslugi/ozelenenie/">
    <img src="/templates/principnovo/img/home_features_processes.png"/>
    <div  style="margin-left:120px; padding-right:20px;">
     Озеленение
    <span>Экология и эстетика участка</span>
    </div>
    <div class="clear"></div>
    </a>
   </div>
   <div class="item">
    <a href="/uslugi/vyrawivanie-rastenij/">
    <img src="/templates/principnovo/img/1111222.png"/>
    <div style="margin-left:120px; padding-right:20px;">
     Выращивание растений
     <span>Растения для вашего участка на заказ.</span>
    </div>
    <div class="clear"></div>
    </a>
   </div>
   <div class="item">
    <a href="/uslugi/prodazha-rastenij/">
    <img src="/templates/principnovo/img/home_features_contact.png"/>
    <div style="margin-left:120px; padding-right:20px;">
     Продажа растений
     <span>Многообразие, качество, удобный сервис.</span>
    </div>
    <div class="clear"></div>
    </a>
   </div>
 
   <div class="item">
    <a href="/uslugi/uhod-za-uchastkom/">
    <img src="/templates/principnovo/img/112233.png"/>
    <div style="margin-left:120px; padding-right:20px;">
     Уход за участком
     <span>Каждый сад требует тщательного ухода, это общеизвестная истина.</span>
    </div>
    <div class="clear"></div>
    </a>
   </div>
   
<div class="clear"></div>
 </div>
</section>

<section class="company">
 <div class="bloks">
   <div class="container">
     <div class="left-grid">
       <h3>Почему именно мы?</h3>
       <p>
Хотите жить, сохраняя лучшие традиции вашей семьи – мы создадим для вас роскошный усадебный сад, которым ваши близкие будут гордиться! Именно этот сад станет олицетворением ваших традиций, ваших ценностей и личного статуса! Мы знаем, как реализовать вашу идеальную мечту о собственном саде, где вы можете устраивать семейные обеды и светские приемы, где ваши дети будут веселиться и расти, где ваша жизнь будет идеальной!
       </p>
       <ul>
       <li>ЕВРОПЕЙСКИЙ ПОДХОД</li>
       <li>СОВРЕМЕННЫЕ ТЕХНОЛОГИИ</li>
       <li>ГАРАНТИИ КАЧЕСТВА</li>
       </ul>
       <p style="padding-top:20px;">
       <a class="grey-button" href="/o-nas/pochemu-imenno-my/">Читать дальше</a>
       </p>
       <p style="padding-top:20px;">
       <a class="grey-button" target="_blank" href="/images/company.pdf">Презентация о компании</a>
       </p>
       <p style="padding-top:20px;">
       <a class="grey-button" target="_blank" href="/images/tehnologi.pdf">Презентация технолологии</a>
       </p>
     </div>
     <div class="right-grid">
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/10-90eddbc404.jpg">
      <img src="/templates/principnovo/img/10-90eddbc404.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/11.jpg">
      <img src="/templates/principnovo/img/11.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/12.jpg">
      <img src="/templates/principnovo/img/12.jpg"/>
      </a>
      <a  class="gallery fen" rel="group" href="/templates/principnovo/img/16.jpg">
      <img src="/templates/principnovo/img/16.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/2.jpg">
      <img src="/templates/principnovo/img/2.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/3.jpg">
      <img src="/templates/principnovo/img/3.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/6.jpg">
      <img src="/templates/principnovo/img/6.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/7.jpg">
      <img src="/templates/principnovo/img/7.jpg"/>
      </a>
      <a class="gallery fen" rel="group" href="/templates/principnovo/img/8.jpg">
      <img src="/templates/principnovo/img/8.jpg"/>
      </a>
     <div class="clear"></div>
     <p style="padding-top:20px;">
       <a class="grey-button" href="/portfolio/">Смотреть портфолио</a>
       </p>
     </div>
<div class="clear"></div>
   </div>
 </div>
</section>

<section class="teem">
<style>
.item-teem { position:relative; text-decoration:none; z-index:10;}
.teem-inf { width:100%; height:100%; position:absolute; bottom:0px; text-align:center; transition:0.7s; background:rgba(0,0,0,0.5); font-size:14px; opacity:0;}
.teem-inf span {font-size:14px; text-transform:uppercase; padding-top:40%;  display:block;}
.item-teem:hover .teem-inf { height:100%; transition:0.7s; opacity:1;}
.tehnology-item a { position:relative; text-decoration:none;  display:block; overflow:hidden;}
.tehnology-item .inf { width:80%; height:0px; position:absolute; bottom:0px; text-align:center; transition:0.7s; font-size:14px; overflow:hidden;}
.tehnology-item a:hover .inf { height:70%; transition:0.7s;}
.tehnology-item:hover img { opacity:0.3!important;}
.news-titles {color:#FFF; text-decoration:none;}
.news-titles:hover { color:#999;}
</style>
 <div class="container">
  <h3>Наша команда</h3>
  <h4>Профессионалы из России, Германии, Франции и Швеции<br />готовы выполнить ваш проект</h4>
<div class="slide">
 <div id="carousel" class="flexslider">
  <xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
</div>   
<div class="clear"></div>
</div>   
 </div>
</section>


<section class="newsblock">
<div class="container">
 <div class="coluns" style="background:rgba(0, 0, 0, 0.28);">
  <span>Технологии</span>
  <div class="tehnology">
   <div class="tehnology-item">
<a href="/uslugi/landshaftnoe-proektirovanie/"><img src="/templates/principnovo/img/home_features_ideas.png"/>
<div class="inf">
Ландшафтное<br />проектирование
</div>
</a>
   </div>
   <div class="tehnology-item">
<a href="/uslugi/landshaftnoe-stroitelstvo/"><img src="/templates/principnovo/img/home_features_strategy.png"/>
<div class="inf">
Ландшафтное<br />строительство
</div>
</a>
   </div>
   <div class="tehnology-item">
<a href="/uslugi/ozelenenie/"><img src="/templates/principnovo/img/home_features_processes.png"/>
<div class="inf">
Озеленение
</div>
</a>
   </div>
   <div class="tehnology-item">
<a href="/shop/"><img src="/templates/principnovo/img/home_features_contact.png"/>
<div class="inf">
Продажа<br />растений
</div>
</a>
   </div>
   <div class="clear"></div>
  </div>
 <!-- <a class="grey-button" style="margin-left:20px;" href="/uslugi/">Все статьи</a> -->
 </div>
 <div class="coluns"  style="background:rgba(0, 0, 0, 0.15);">
  <span>Контакты</span>
  <div class="info">
  Санкт-Петербург,<br />Выборгское шоссе, д.365
  </div>
  <div class="info">
  Звоните нам: 8 (921) 952 91 90
  </div>
  <div class="info">
  Звоните нам: 8 (812) 702 92 93
  </div>
  <div class="info">
  </div>
 </div>
 <div class="coluns"  style="background:rgba(0, 0, 0, 0.15);">
  <span>Последние события</span>
  <xsl:apply-templates select="document('udata://news/lastlist/(/o-nas/sobytiya/)/notemplate/2/0')" mode="right-column-news" />
 </div>
 <div class="coluns"  style="background:rgba(0, 0, 0, 0.1);">
  <span>Отзывы</span>
  
   <div class="info" style="padding-right:15px;">
  Людмила<br />
  <a class="news-titles" style="display: block;
    font-size: 12px;
    padding-top: 5px;" href="/o-nas/otzyvy/">Большое спасибо менеджерам по продажам. Обслуживание на высшем уровне.</a>
  </div>
  <div class="info" style="padding-right:15px;">
  Рукавишников Андрей<br />
   <a class="news-titles" style="display: block;
    font-size: 12px;
    padding-top: 5px;" href="/o-nas/otzyvy/">Современный подход к работе, очень порадовало качество и вежливость.</a>
  </div>
  <div class="info">
  </div>
 </div>
 <div class="clear"></div>
</div> 
</section>
<section class="maps">
<xsl:value-of select=".//property[@name = 'yandeks']/value" disable-output-escaping="yes" />
</section>
<!-- главная -->
</xsl:when>
<xsl:when test="/result[@method = 'search_do']">
<section id="mine">
<style>
.sera-res { width:23%; margin-left:1%; margin-right:1%; float:left; margin-bottom:20px;}
.imges-res img { width:auto; height:200px;}
</style>
<div class="container">
 <xsl:apply-templates select="result" /> 
</div>
</section>
</xsl:when>
<xsl:when test="result/@request-uri = '/kontakty/'">
<section id="mine">
 <div class="container">
  <div class="left-col">
     <xsl:apply-templates select="result" mode="header" />
     <div class="content" style="padding-top:20px;">
      <xsl:apply-templates select="$errors" />
      <div style="width:58%; margin-right:2%; float:left;">
      <xsl:apply-templates select="result" />
      </div>
<div style="width:40%; float:left;">


<div class="border" style="margin-bottom:20px;">
<h4 style="margin-top:10px; font-size:16px;">Связаться с нами</h4>
<form xmlns="http://www.w3.org/1999/xhtml" method="post" action="/webforms/send/" onsubmit="site.forms.data.save(this); return site.forms.data.check(this);" enctype="multipart/form-data"><input type="hidden" name="system_email_to" value="687"/>
<div><input type="text" style="width:90%; height:30px; margin-bottom:10px;" placeholder="Ваше имя" name="data[new][vashe_imya]" value="" class="textinputs"/></div>
<div><input type="text"  style="width:90%; height:30px; margin-bottom:10px;" placeholder="Ваш телефон" name="data[new][kontaktnyj_telefon]" value="" class="textinputs"/></div>
<div><textarea type="text"  style="width:90%; height:50px; margin-bottom:10px;"  placeholder="Ваш вопрос" name="data[new][naimenovanie_vakansii]" value="" class="textinputs"/></div>
<input type="hidden" name="system_form_id" value="321"/>
<input type="hidden" name="ref_onsuccess" value="/webforms/posted/321/"/>
<input type="submit" class="green-button" value="Отправить"/>
</form>
</div>
</div>
      <div class="clear"></div>
     </div> 
  </div>
  
<div class="right-col">
<xsl:apply-templates select="document('udata://content/menu/0/2/(uslugi)')/udata" mode="left_menu" />
</div>
<div class="clear"></div>
</div>
</section>
<section id="mine">
 <div class="container">
  <div class="left-col">
     <h1><xsl:apply-templates select=".//property[@name = 'picalevo_h1']/value" /></h1>
     <div class="content" style="padding-top:20px;">
      <xsl:apply-templates select="$errors" />
      <div style="width:58%; margin-right:2%; float:left;">
     <xsl:value-of select=".//property[@name = 'picalevo_kon']/value" disable-output-escaping="yes" />
      </div>
<div style="width:40%; float:left;">
<div class="border" style="margin-bottom:20px;">
<!--<h4 style="margin-top:10px; font-size:16px;">Связаться с нами</h4>
<div><input type="text" style="width:90%; height:30px; margin-bottom:10px;" placeholder="Ваше имя" name="data[696][fname]" value="" class="textinputs"/></div>
<div><input type="text"  style="width:90%; height:30px; margin-bottom:10px;" placeholder="Ваш телефон" name="data[696][email]" value="" class="textinputs"/></div>
<div><textarea type="text"  style="width:90%; height:50px; margin-bottom:10px;"  placeholder="Ваш вопрос" name="data[696][phone]" value="" class="textinputs"/></div>
<a class="green-button" href="#">Отправить</a>-->
</div>
</div>
      <div class="clear"></div>
     </div> 
  </div>
  
<div class="right-col">

</div>
<div class="clear"></div>
</div>
</section>
<section class="maps">
<xsl:value-of select="document('upage://home.yandeks')//value" disable-output-escaping="yes" />
</section>
</xsl:when>


<xsl:when test="result/@request-uri = '/predlozheniya-i-akcii/'">
<section id="mine">
 <div class="container">
  <div class="left-col">
     <xsl:apply-templates select="result" mode="header" />
     <div class="content" style="padding-top:20px;">
      <xsl:apply-templates select="$errors" />
      <xsl:apply-templates select="document('usel://special-offers/?limit=&specials-limit;')" mode="special-offers" />
      <div class="clear"></div>
      <xsl:apply-templates select="result" />
      <div class="clear"></div>
     </div> 
  </div>
  
<div class="right-col">
<!-- <xsl:apply-templates select="document('udata://emarket/cart')" mode="basket" /> -->
<!-- <xsl:apply-templates select="document('udata://content/menu/0/2/(shop)')/udata" mode="catalog_left_menu"/> -->
	<!-- <xsl:apply-templates select="document(concat('udata://content/menu/0/2/',result/page/@id))/udata" mode="left_menu" /> -->
	<xsl:apply-templates select="document('udata://menu/draw/top_menu')/udata/item[@link='/predlozheniya-i-akcii/']" mode="right_menu_draw" />
</div>
<div class="clear"></div>
</div>
</section>
</xsl:when>


<!-- Список услуг (Лента новостей - Лэндинг) -->

<xsl:when test="result//page/@id = 1786">
	<div class="landing-container">
		<xsl:apply-templates select="$errors" />
		<xsl:apply-templates select="result" />
		<div class="clear"></div>
        <!-- Modal form -->
        <div class="pop-up">
            <span class="modal_close">X</span>
            <p>Звонок на данный номер заказан, Вам перезвонят в течении 10 минут</p>
        </div>
        <div class="overlay"></div><!-- Подложка -->
		<div class="clear"></div>
	</div>
</xsl:when>


<!-- внутряк -->

<xsl:otherwise>
<section id="mine">
<style>
.left-col .check li { background: url(/templates/principnovo/img/list_check.png) no-repeat left top;
    list-style:none;
    margin-bottom: 10px;
    padding:0px 0px 6px 30px;
    color:#333;  margin-left:0px;}
.left-col li {color:#333; list-style:outside; margin-left:20px;}
p {color:#333;}
</style>
 <div class="container">
<xsl:choose>
<xsl:when test="result/@request-uri = '/o-nas/vakansii/'">
  <div class="left-col">
  <script>
			function tabChange(tab, prefix) {
				var i, tabs = tab.parentNode;
				for (i=0; tabs.childNodes.length > i; i++) {
					var tab_in = tabs.childNodes[i];
					if (tab_in.nodeName == "#text") continue;
					tab_in.className = "";
				}
				tab.className = "act";
				var con_tabs = jQuery('.' + prefix + tabs.className);
				var con_tabs_arr = con_tabs[0].childNodes;
				for (i=0; con_tabs_arr.length > i; i++) {
					var con_tab = con_tabs_arr[i];
					if (con_tab.nodeName == "#text") continue;
					con_tab.style.display = "none";
				}
				var con_tab_act = document.getElementById(prefix + tab.id);
				con_tab_act.style.display = "block";
			}
		</script>


     <xsl:apply-templates select="result" mode="header" />
     <div class="content" style="padding-top:20px;">
      <xsl:apply-templates select="$errors" />
<div class="tabs">
<span id="tab_new4" class="act" onclick="tabChange(this, 'con_');"><xsl:value-of select=".//property[@name = 'nazvanie4']/value" disable-output-escaping="yes" /></span>
<span id="tab_new" onclick="tabChange(this, 'con_');"><xsl:value-of select=".//property[@name = 'nazvanie']/value" disable-output-escaping="yes" /></span>
<span id="tab_new2" onclick="tabChange(this, 'con_');"><xsl:value-of select=".//property[@name = 'nazvanie2']/value" disable-output-escaping="yes" /></span>
<span id="tab_new3" onclick="tabChange(this, 'con_');"><xsl:value-of select=".//property[@name = 'nazvanie3']/value" disable-output-escaping="yes" /></span>
</div>
<div style="clear:both;"></div>
<div class="con_tabs">
<div id="con_tab_new4" style="display: block;">
<xsl:value-of select=".//property[@name = 'opisanie4']/value" disable-output-escaping="yes" />
</div>
<div id="con_tab_new" style="display:none;">
<xsl:value-of select=".//property[@name = 'opisanie']/value" disable-output-escaping="yes" />
</div>
<div id="con_tab_new2" style="display:none;">
<xsl:value-of select=".//property[@name = 'opisanie2']/value" disable-output-escaping="yes" />
</div>
<div id="con_tab_new3" style="display:none;">
<xsl:value-of select=".//property[@name = 'opisanie3']/value" disable-output-escaping="yes" />
</div> 
</div>     
<style>
.tabs span {display: inline-block;
padding: 5px 10px;
background:#CCC; font-size:16px; cursor:pointer; margin-left:1px;}
.tabs span:hover {
background:#82B064;}
.tabs .act {background: #82B064;}
.con_tabs { border:1px solid #CCC; padding:10px;}
	  .rezume { margin-top:40px;}
	  .rezume form span { display:block;}
	  .rezume .textinputs { width:250px; height:30px;}
	  .rezume .button {background: #82B064;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2) inset;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
  padding: 8px 20px;
  text-decoration: none;
  color: #FFF;
  display: inline-block;
  cursor: pointer;
  border: none;}
	  </style>
      <div class="rezume">
      <h1>Отправить резюме</h1>
      <div style="margin-bottom: 10px;
font-size: 18px;">Телефон отдела кадров: 8 (812) 702-92-93 ПН-ПТ с 10-00 до 18-00</div>
<xsl:apply-templates select="document('udata://webforms/add/321')" />
      </div>
      <div class="clear"></div>
     </div> 
  </div>
</xsl:when>
<xsl:otherwise>
<div class="left-col">
     <xsl:apply-templates select="result" mode="header" />
     <div class="content" style="padding-top:20px;">
      <xsl:apply-templates select="$errors" />
      <xsl:apply-templates select="result" />
       <div class="clear"></div>
      <div style="margin-bottom:20px;">
      <xsl:value-of select=".//property[@name = 'partners']/value" disable-output-escaping="yes" />
      </div>
      <div class="clear"></div>
     </div> 
  </div>
</xsl:otherwise>
</xsl:choose> 
<div class="right-col">
<xsl:choose>
<xsl:when test="/result[@method = 'category']">
<div style="margin-bottom:15px;">
<xsl:call-template name="search-form-left-column" />
</div>
<xsl:apply-templates select="document('udata://emarket/cart')" mode="basket" />
<xsl:apply-templates select="document('udata://content/menu/0/2/(shop)')/udata" mode="catalog_left_menu"/>
<xsl:choose>
<xsl:when test="document(concat('upage://', $document-page-id,'.hvo'))/udata//value = 1">
<xsl:variable name="type" select="document(concat('udata://catalog/getObjectsList/', page/@id, '////3/'))/udata/type_id"/>
<xsl:apply-templates select="document(concat('udata://catalog/search////', $type))"/>
</xsl:when> 
<xsl:when test="document(concat('upage://', $document-page-id,'.listv'))/udata//value = 1">
<style>
	.catalog_filter .left span { display:block;}
	.catalog_filter .left select { width:95%; height:25px; margin-bottom:20px;}
	.catalog_filter h2 {border-bottom: 1px solid #ccc;
    color: #666;
    display: block;
    font-size: 14px;
    padding: 10px 0;
    text-decoration: none;
    text-transform: uppercase;}
	</style>
<form class="catalog_filter">
<h2>Фильтр по параметрам:</h2>
<div class="left">
<div><label><span>Маx высота</span><select name="fields_filter[vysota_derev_ya]"><option></option>
<option value="2247">Кустовые</option>
<option value="2248">Плакучие и привитые на штамб</option>
<option value="2249">Стандартные</option>
<option value="2246">высокие выше 1,5м</option>
<option value="2251">низкорастущие  до 0,5м</option>
<option value="2250">средние от 0,5 до 1,5 м</option></select></label></div>
<div><label><span>Декоративность</span><select name="fields_filter[dekorativnost]"><option></option>
<option value="362">Декоративнолиственные</option>
<option value="2245">Декоративноплодные</option>
<option value="354">Красивоцветущие</option></select></label></div>
</div>
<div class="right"></div>
<div class="clear"></div>
<div>
<input type="submit" value="Применить" class="button groow-button" /><input type="button" onclick="javascript: window.location = '?';" value="Сбросить" class="button groow-button" />
</div>
</form>

</xsl:when>

  
<xsl:otherwise>
<div></div>
</xsl:otherwise>
</xsl:choose> 
</xsl:when>
<xsl:when test="/result[@method = 'object']">
<div style="margin-bottom:15px;">
<xsl:call-template name="search-form-left-column" />
</div>
<xsl:apply-templates select="document('udata://emarket/cart')" mode="basket" />
<xsl:apply-templates select="document('udata://content/menu/0/2/(shop)')/udata" mode="catalog_left_menu"/>
<div class="border" style="padding-top:0px; padding-bottom:20px; margin-bottom:20px;">
<style>
.border .image img { width:100%; height:auto;}
</style>

<h4 style=" border-bottom: 1px solid #ccc;
    color: #090;
    font-family: verdana;
    font-size: 16px;
    font-weight: 400;
    margin-bottom: 20px;
    padding-bottom: 5px;">Сопутствующие товары</h4>
<xsl:apply-templates select=".//property[@name = 'rekomenduem']/value" mode="rec-view" />
</div>
</xsl:when>
<xsl:when test="/result[@method = 'rubric']">
<xsl:apply-templates select="document('udata://content/menu/0/2/')/udata" mode="catalog_left_menu_menu"/>
</xsl:when>
<xsl:when test="/result[@method = 'item']">
<xsl:apply-templates select="document('udata://content/menu/0/2/')/udata" mode="catalog_left_menu_menu"/>
</xsl:when>
<xsl:when test="/result/parents/page/@id = '4'">
	<xsl:apply-templates select="document('udata://menu/draw/top_menu')/udata/item[@link='/predlozheniya-i-akcii/']" mode="right_menu_draw" />
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="document('udata://content/menu/0/2/')/udata" mode="catalog_left_menu_menu"/>
<xsl:apply-templates select="document('udata://content/menu/0/2/(uslugi)')/udata" mode="left_menu" />
</xsl:otherwise>
</xsl:choose> 
  </div>
 <div class="clear"></div>
</div>
</section>

</xsl:otherwise>
</xsl:choose>



<!-- внутряк -->
<!-- <xsl:choose>
<xsl:when test="/result[@method = 'category']">
<footer style="padding-bottom:0px;">
<div class="container">
<div class="footer-cols" style="width:30%; float:left; margin-right:5%; color:#FFF;">
<div style="margin-bottom:5px; font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Каталог</div>
<xsl:apply-templates select="document('udata://content/menu/0/2/(shop)')/udata" mode="bottom_menu_cat" />
</div>
<div class="footer-cols" style="width:30%; float:left;  margin-right:5%; color:#FFF;">
<div style="margin-bottom:5px;  font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Информация</div>
<a style="display:block; padding-bottom:5px; border-bottom:" href="/info/">О магазине</a>
<xsl:apply-templates select="document('udata://content/menu/0/2/(info)')/udata" mode="bottom_menu_cat" />
</div>
<div class="footer-cols" style="width:30%; float:left; color:#FFF;margin-right:0;">
<div style="margin-bottom:5px;  font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Контакты</div>
Адрес: г. Санкт-Петербург,<br/> 
Выборгское шоссе д.365 <br/>
Телефон: 8 (812) 702-92-93 <br/>
Телефон: 8 (921) 952-91-90 <br/>
Время работы: ПН - ПТ с 10 до 19
</div>
<div class="clear"></div>
</div>
<div style="margin-top:20px; padding:5px; background:#393431; text-align:right;">
 <div class="container" style="color:#CCC;">
   <xsl:text>&#169; PRINCIP NOVO 2015</xsl:text>
 </div>
</div>
</footer> 
</xsl:when>
<xsl:when test="/result[@method = 'object']">
<footer style="padding-bottom:0px;">
<div class="container">
<div class="footer-cols" style="width:30%; float:left; margin-right:5%; color:#FFF;">
<div style="margin-bottom:5px; font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Каталог</div>
<xsl:apply-templates select="document('udata://content/menu/0/2/(shop)')/udata" mode="bottom_menu_cat" />
</div>
<div class="footer-cols" style="width:30%; float:left;  margin-right:5%; color:#FFF;">
<div style="margin-bottom:5px;  font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Информация</div>
<a style="display:block; padding-bottom:5px; border-bottom:" href="/info/">О магазине</a>
<xsl:apply-templates select="document('udata://content/menu/0/2/(info)')/udata" mode="bottom_menu_cat" />
</div>
<div class="footer-cols" style="width:30%; float:left; color:#FFF;margin-right:0;">
<div style="margin-bottom:5px;  font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Контакты</div>
Адрес: г. Санкт-Петербург,<br/> 
Выборгское шоссе д.365 <br/>
Телефон: 8 (812) 702-92-93 <br/>
Телефон: 8 (921) 952-91-90 <br/>
Время работы: ПН - ПТ с 10 до 19
</div>
<div class="clear"></div>
</div>
<div style="margin-top:20px; padding:5px; background:#393431; text-align:right;">
 <div class="container" style="color:#CCC;">
   <xsl:text>&#169; PRINCIP NOVO 2015</xsl:text>
 </div>
</div>
</footer> 
</xsl:when>
<xsl:when test=".//property[@name = 'informacionnaya_stranichka']/value = 2">
<footer style="padding-bottom:0px;">
<div class="container">

<div class="footer-cols" style="">
<div style="margin-bottom:5px; font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Каталог</div>
<xsl:apply-templates select="document('udata://content/menu/0/2/(shop)')/udata" mode="bottom_menu_cat" />
</div>
<div class="footer-cols" style="">
<div style="margin-bottom:5px;  font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Информация</div>
<a style="display:block; padding-bottom:5px; border-bottom:" href="/info/">О магазине</a>
<xsl:apply-templates select="document('udata://content/menu/0/2/(info)')/udata" mode="bottom_menu_cat" />
</div>
<div class="footer-cols" style="">
<div style="margin-bottom:5px;  font-size:20px; padding-bottom:5px; border-bottom:1px solid #666;">Контакты</div>
Адрес: г. Санкт-Петербург,<br/> 
Выборгское шоссе д.365 <br/>
Телефон: 8 (812) 702-92-93 <br/>
Телефон: 8 (921) 952-91-90 <br/>
Время работы: ПН - ПТ с 10 до 19
</div>
<div class="clear"></div>
</div>
<div style="margin-top:20px; padding:5px; background:#393431; text-align:right;">
 <div class="container" style="color:#CCC;">
   <xsl:text>&#169; PRINCIP NOVO 2015</xsl:text>
 </div>
</div>
</footer> 
</xsl:when>
<xsl:when test=".//property[@name = 'informacionnaya_stranichka']/value = 1"> -->
<footer style="padding-bottom:0px;">
<div class="container">	
	<xsl:apply-templates select="document('udata://menu/draw/footer_menu')/udata" mode="bottom_menu_draw" />
	<div class="footer-cols" style="">
		<div class="footer-menu-1">Контакты</div>
		Адрес: г. Санкт-Петербург,<br/> 
		Выборгское шоссе д.365 <br/>
		Телефон: 8 (812) 702-92-93 <br/>
		Телефон: 8 (921) 952-91-90 <br/>
		Время работы: ПН - ПТ с 10 до 19
	</div>
	<div class="clear"></div>
</div>
<div style="margin-top:20px; padding:5px; background:#393431; text-align:right;">
 <div class="container" style="color:#CCC;">
   <xsl:text>&#169; PRINCIP NOVO 2015</xsl:text>
 </div>
</div>
</footer> 
<!-- </xsl:when>
<xsl:otherwise>
<footer style="padding-bottom:10px;">
<div class="container">
<div style="float:left; width:70%; text-transform:uppercase;">
<xsl:apply-templates select="document('udata://content/menu/')" mode="bottom_menu" />
</div>
<div style="float:left; width:30%; color:#FFF; text-align:right; position:relative;">
<xsl:text>&#169; PRINCIP NOVO 2015</xsl:text>
<a class="scrollto" href="#top"></a>
</div>
<div class="clear"></div>
<style>
.created { text-decoration:none; color:#CCC;}
.created:hover { text-decoration:none; color:#090;}
</style>
<div style="color:#ccc; padding-top:15px; text-align:left;">
Сreated by <a href="http://webforester.ru/" class="created" rel="nofollow"> WebForester</a></div>
</div>

<div class="clear"></div>
</footer>
</xsl:otherwise>
</xsl:choose>  -->

<div id="kab" style="display:none">
<xsl:apply-templates select="/result/user" />
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter28390606 = new Ya.Metrika({id:28390606,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<!-- /Yandex.Metrika counter -->
			</body>
		</html>
	</xsl:template>
<xsl:template match="udata[@method = 'navibar']">
<div class="container">
  <a href="/">Главная</a>
  <xsl:text> » </xsl:text>
  <xsl:apply-templates select="items/item" mode="navibar"/>
</div>
</xsl:template>
<xsl:template match="item" mode="navibar">
  <a href="{@link}"><xsl:value-of select="."/></a>
  <xsl:text> » </xsl:text>
</xsl:template>
<xsl:template match="item[position() = last()]" mode="navibar">
    <span><xsl:value-of select="."/></span>
</xsl:template>


<!-- Шаблоны для главной -->


<xsl:template match="item" mode="main-uslugi">
	<xsl:variable name="u_page" select="document(concat('upage://',.//property[@name='u_page']//page/@id))/udata" />
	<!-- <xsl:copy-of select="$u_page"/> -->
	<div class="item">
		<a href="{$u_page/page/@link}">
			<img src="{$u_page//property[@name='menu_pic_ua']/value}"/>
			<xsl:if test=".//property[@name='menu_pic_ua']">
				<xsl:attribute name="src"><xsl:value-of select=".//property[@name='menu_pic_ua']/value"/></xsl:attribute>
			</xsl:if>
			<div style="margin-left:120px; padding-right:20px;">
				<xsl:value-of select=".//property[@name='h1']/value"/>
				<span><xsl:value-of select=".//property[@name='short_description']/value" disable-output-escaping="yes"/></span>
			</div>
			<div class="clear"></div>
		</a>
	</div>
    <!-- <xsl:copy-of select="."/> -->
</xsl:template>

<xsl:template match="item" mode="main-slider">
	<li>
		<!-- <xsl:copy-of select="."/> -->
		<div style="position:absolute; top:60%; left:20%; z-index:10; text-align:left;">
			<div style="background:rgba(130,176,100,0.8) url(/templates/principnovo/img/raster_bg.png);
			color:#FFF!important;
			font-weight: 400;
			padding:8px 20px;
			font-size: 32px; margin-bottom:10px; display:inline-block;">
				<xsl:value-of select="@name" disable-output-escaping="yes"/>
			</div>
			<br/>
			<div style="background:rgba(141,121,106,0.85) url(/templates/principnovo/img/raster_bg.png);
			color:#FFF!important;
			font-weight: 400;
			padding:8px 20px;
			font-size: 24px; display:inline-block;">
				<xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
			</div>
		</div>
		<img src="{.//property[@name='menu_pic_ua']/value}" alt="" height="450"/>
	</li>	
	
</xsl:template>

<xsl:template match="item" mode="main-gal">
	<xsl:if test=".//property[@name='menu_pic_ua']">
		<a class="gallery fen" rel="group" href="{.//property[@name='menu_pic_ua']/value}">
			<img src="{.//property[@name='menu_pic_ua']/value}"/>
		</a>
	</xsl:if>			
</xsl:template>

<xsl:template match="udata" mode="main-team">
	<xsl:param name="slide" select="1" />
	<li>		
		<xsl:apply-templates select="./items/item[position() &lt; $slide*4+1 and position() &gt; ($slide - 1)*4]" mode="main-team-item" />
	</li>
	<xsl:if test="$slide*4 &lt; count(items/item)">
		<xsl:apply-templates select="." mode="main-team" >
			<xsl:with-param name="slide" select="$slide+1" />
		</xsl:apply-templates>
	</xsl:if>			
</xsl:template>

<xsl:template match="item" mode="main-team-item">	
	<div class="items-teem">
		<a class="item-teem" href="/o-nas/"> 
			<img src="{.//property[@name='menu_pic_ua']/value}" alt="" />
		</a>
		<div class="teem-inf">
			<span>
				<xsl:value-of select="@name"/>
			</span> 
			<xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
		</div>
	</div>				
</xsl:template>

</xsl:stylesheet>