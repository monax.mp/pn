$(document).ready(function() { 

	// modal window
	/*$('.landing-container .call').click( function(event){ 
		event.preventDefault(); 

		var $heightForm = $('.landing-container .pop-up').outerHeight();;
		var $heightWindow = $(window).height();
		var $scrollTop = $(window).scrollTop();
		var $topPopup = (($heightWindow / 2) + $scrollTop) - ($heightForm / 2);

		$('.landing-container .overlay').fadeIn(400, 
			function(){ 
				$('.landing-container .pop-up') 
					.css('display', 'block') 
					.animate({
						opacity: 1, 
						top:  $heightWindow > $heightForm ? $topPopup : '50px'
					}, 200); 
		});
	});*/

	$('.landing-container .modal_close, .landing-container .overlay').click( function(){
		$('.landing-container .pop-up')
			.animate({opacity: 0}, 200,  
				function(){ 
					$(this).css('display', 'none'); 
					$('.landing-container .overlay').fadeOut(400); 
				}
			);
	});

	$('.landing-design .block-3 > a').click( function(event){
		event.preventDefault(); 
		$(this).parent('.block-3').children('img').attr('src',$(this).attr('href'));
	});

	// accordion
	var $_li = $('.landing-questions ul > li:even');
	var $_p = $('.landing-questions ul > li:odd');
	//$_p.eq($_p.length - 1).show();
	$_li.click(function(){
		$(this).next("li").slideToggle("slow")
			.siblings("li:visible").not($_li).slideUp("slow");
	});

	// fancybox    
	$("a.fancybox").fancybox();
	$("a.landing-gallery").fancybox();
	$("a.landing-gallery2").fancybox();
	$("a.landing-gallery3").fancybox();
	$("a.fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
        helpers: {
            thumbs: {
                width  : 100,
                height : 100,
                
            }
        }
    });

		

	
	$(".landing-form form").submit(function(event){
		event.preventDefault(); 
		$(this).find('.alarm').removeClass('alarm');
		/*var required_fields = $(this).find(':required');
		for (key in required_fields){
			var elem = required_fields[key];
			if (elem.value == ''){
				$(elem).addClass('alarm');					
			}
		}
		if ($(this).find('.alarm').length != 0) return false;
		return true;*/
		var $lform = $(this);
		$lform.find('input.sitepagelink').val($('.landing-menu .item.active a').attr('href'));
		$lform.find('input.sitepage').val($('.landing-menu .item.active a').text());
		$.ajax({
			type: 'POST',
			url: $lform.attr('action'),
			data: $lform.serialize(),
			dataType: 'html'
		}).done(function(data, textStatus, jqXHR) {
			var $errors = $(data).find('.errors ul li');
			if ($errors.length) {
				errors_html = $errors.html();
			}
			if ($errors.length && !/успешно/.test(errors_html) && !/отправлено/.test($(data).find('title').html())) {
				if (/e-mail/.test(errors_html)) {
					$error_field = $lform.find('input[name="data[new][email]"]');
				} else {
					$error_field = $lform.find('input:required').last();
				}
				$error_field.val('').attr('placeholder',errors_html).addClass('alarm');
				//$lform.find('.captcha_img').attr('src',$(data).find('.captcha_img').attr('src'));
			} else {
				$('.landing-container .pop-up p').html($lform.parent().parent().find('.form-success').html());
				var $heightForm = $('.landing-container .pop-up').outerHeight();
				var $heightWindow = $(window).height();
				var $scrollTop = $(window).scrollTop();
				var $topPopup = (($heightWindow / 2) + $scrollTop) - ($heightForm / 2);
				$('.landing-container .overlay').fadeIn(400, 
					function(){ 
						$('.landing-container .pop-up') 
							.css('display', 'block') 
							.animate({
								opacity: 1, 
								top:  $heightWindow > $heightForm ? $topPopup : '50px'
							}, 200); 
				});
				$lform.trigger("reset");
			}
		});
	});
});